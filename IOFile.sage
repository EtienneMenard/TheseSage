# coding: utf8

def fonctionCoucouIOFile():
	print("Fichier IOFile chargé")
	
fonctionCoucouIOFile()

def creerRepertoireLog():
	"""
		Vérifie s'il existe un répertoire log et le crée sinon
	"""
	if not(os.path.exists(os.path.expanduser(cheminCourant+'log'))):
		os.mkdir(os.path.expanduser(cheminCourant+'log'))
		print("Dossier"+os.path.expanduser(cheminCourant+'log')+" créé")
	else :
		print("Le dossier "+os.path.expanduser(cheminCourant+'log')+" existe déjà")

def recupererDate():
	"""
		Récupère la date et l'heure courante et renvoie une chaine de 
		caractères correctement formatée
	"""
	import datetime
	now=datetime.datetime.now()
	res=now.strftime("%Y_%m_%d_%H_%M_%S")
	return res

def creerFichierLog():
	"""
		Crée un fichier de log et renvoie le pointeur vers le fichier
	"""
	nomDeFichier=os.path.expanduser(cheminCourant+"log/"+recupererDate()+".log")
	f=open(nomDeFichier,'a')
	f.write("Fichier crée le "+recupererDate())
	return([f,nomDeFichier])

def repertoireSauvegarde(repw,v):
	"""
		Crée un dossier pour sauvegarder les résultats d'un test
	"""
	creerRepertoireLog()#On crée le répertoire log
	typeCartan=W.cartan_type()[0]+str(W.cartan_type()[1])+'/'
	if not(os.path.exists(os.path.expanduser(cheminCourant+'log/figures/'))):#On crée le dossier
		#figures si nécessaire
		os.mkdir(os.path.expanduser(cheminCourant+'log/figures/'))
		print("Dossier"+os.path.expanduser(cheminCourant+'log/figures/')+" créé")
	else :
		print("Le dossier "+os.path.expanduser(cheminCourant+'log/figures/')+" existe déjà")
	if not(os.path.exists(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan))):#Puis le 
		#dossier au nom du type de Dynkin.
		os.mkdir(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan))
		print("Dossier"+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan)+" créé")
	else :
		print("Le dossier "+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan)+" existe déjà")
	if not(os.path.exists(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)))):#Puis le 
		#sous dossier repw=...
		os.mkdir(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)))
		print("Dossier"+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw))+" créé")
	else :
		print("Le dossier "+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw))+" existe déjà")
	if not(os.path.exists(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)+'/v='+str(v)))):
		#et le sous-sous dossier v=
		os.mkdir(os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)+'/v='+str(v)))
		print("Dossier"+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)+'/v='+str(v))+" créé")
	else :
		print("Le dossier "+os.path.expanduser(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)+'/v='+str(v))+" existe déjà")
	return(cheminCourant+'log/figures/'+typeCartan+'repw='+str(repw)+'/v='+str(v)+'/')
