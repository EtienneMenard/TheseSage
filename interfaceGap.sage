# coding: utf8

def fonctionCoucouInterfaceGap():
	print ("Fichier interfaceGap.sage chargé")
	
fonctionCoucouInterfaceGap()

def calculMouvementsTresses(rep1,rep2,W):
	"""
		Calcule les mouvements de tresses pour passer d'un représentant 
		rep1 d'un mot d'un groupe de Weyl W à un autre rep2 du même mot.
		Renvoie la liste des mouvements à faire.
	"""
	return(GetBraidRelations(rep1,rep2,W))#Fonction inutile, conservée pour
	#des questions de nom de fonction

def choixSousMot(W,repw):
	"""
		Renvoie aléatoirement un des sous-mots du mot de représentant 
		repw dans le groupe W
	"""
	[type,indice]=W.cartan_type()
	repwcorrige=deepcopy(repw)
	if type=='D':#Gap et Sage n'ont pas la même numérotation en type D
		for i in range(len(repw)):
			repwcorrige[i]=indice-repwcorrige[i]+1
	commande="Random(Random(BruhatSmaller(CoxeterGroup("+'"'+str(type)+'",'+str(indice)+"),EltWord(CoxeterGroup("+'"'+str(type)+'",'+str(indice)+"),"+str(repwcorrige)+"))));"
	#On récupère un élément au hasard dans les sous mots de w
	a=gap3(commande)
	listeGap=list(gap3('CoxeterWord(CoxeterGroup("'+str(type)+'",'+str(indice)+'),'+str(a)+');'))
	listeSage=[]
	for i in listeGap:
		if type=='D':
			listeSage=listeSage+[indice-int(i)+1]
			#Toujours la même correction d'indices
		else:
			listeSage=listeSage+[int(i)]
	return(repVersMot(listeSage,W))
	
def GetBraidRelations(rep1,rep2,W):
	"""
		Donne une suite de mouvements de tresses permettant de passer de rep1
		à rep2, deux représentants du même mot dans W.
	"""
	if rep1==rep2:
		return[]
	if repVersMot(rep1,W)!=repVersMot(rep2,W):
		print("Mots non égaux")
		return False
	n=len(rep1)
	i=rep1[0]
	j=rep2[0]
	if i==j:
		return GetBraidRelations(rep1[1:],rep2[1:],W)
	R=RootSystem(W)
	M=CartanMatrix(R)
	ipij=M[i-1][j-1]
	ipji=M[j-1][i-1]
	if ipij==0:#ij=ji
		u=ExchangeElement(rep1,W,j)#On utilise la condition d'échange
		#u commence par ji
		liste2=GetBraidRelations(u[1:],rep2[1:],W)#Hérédité
		u[0]=i
		u[1]=j#u commence maintenant par ij
		liste1=GetBraidRelations(rep1[1:],u[1:],W)#Hérédité
		liste=liste1+[(n-1,n)]+liste2#On concatène les deux suites de mouvements
		#en rajoutant le mouvement via u
	elif ipij==-1 and ipji==-1:#iji=jij
		u=ExchangeElement(rep1,W,j)
		u=ExchangeElement(u,W,i)
		#u finit par iji
		liste1=GetBraidRelations(rep1[1:],u[1:],W)
		u[2]=j
		u[1]=i
		u[0]=j
		liste2=GetBraidRelations(u[1:],rep2[1:],W)
		liste=liste1+[(n-2,n-1,n)]+liste2
	else:
		print("Type non géré")
		return False
	return(liste)
	
def ExchangeElement(rep,W,lettre):
	"""
		Renvoie un représentant du même mot que rep dans W qui commence 
		à droite par lettre
	"""
	#Sûrement optimisable
	mot=repVersMot(rep,W)
	if (W.simple_reflections()[lettre]*mot).length()>mot.length():
		print("La condition d'échange ne s'applique pas")
		return False
	reptmp=rep
	for i in range(len(rep)):
		reptmp=[lettre]+rep[:i]+rep[i+1:]
		if repVersMot(reptmp,W)==mot:
			return reptmp
	print("Problème")
	return False
