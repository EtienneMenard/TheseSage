# coding: utf8

#ToDoList #Problème : mutaStratesListeDeltaVecteurs ne tient pas compte des suppressions de modules. Ecrire un autre algorithme qui supprimerait les flèches des projectifs une fois supprimés

def fonctionCoucouMutationStrates():
	print("Fichier mutationStrates.sage chargé")
	
fonctionCoucouMutationStrates()

####Fonctions sur w

def compteLettre(mot,lettre,limite=-1):
	"""
		Compte le nombre d'occurence de la lettre dans le mot (donné comme liste). 
		Si un paramètre est précisé pour limite, c'est le nombre d'occurences
		de la lettre strictement à droite de la position choisie.
	"""
	compteur=0
	if limite==1:
		return 0 #Dans ce cas là on considère le mot vide
	if limite!=-1 and limite<=len(mot):
		mot1=mot[-limite+1:]#Ici en fait on compte sur le sous-mot à droite
	else:
		mot1=copy(mot)#Par consistence dans ce qui suit, on travaille sur mot1.
		#Il faudrait optimiser dans l'idéal
	for i in mot1:
		if i==lettre:
			compteur=compteur+1
	return compteur
		
def rangMin(mot, lettre):
	"""
		Retourne la première occurrence à droite de lettre dans mot
	"""
	k=len(mot)
	for i in range(k):
		if mot[-(i+1)]==lettre:
			return i+1
			
def rangMax(mot, lettre):
	"""
		Retourne la dernière occurrence à droite de lettre dans mot.
		En fait, elle est calculée comme la première à gauche.
	"""
	k=len(mot)
	for i in range(k):
		if mot[i]==lettre:
			return k-i
			
def kplus(w,k):
	"""
		Renvoie l'index de l'occurrence suivante de la kè lettre (entier)
		dans le	représentant du mot w (liste d'entiers)
	"""
	n=len(w)
	for i in range(k+1,n+1):
		if w[-i]==w[-k]:
			return(i)
	return(n+1)
	
def kmoins(repw,k):
	"""
		Renvoie l'index de l'occurrence précédente de la kè lettre (entier)
		dans le	représentant du mot w (liste d'entiers)
	"""
	n=len(repw)
	indice=-1
	for i in range(1,k):
		if repw[-(k-i)]==repw[-k]:
			return((k-i))
	return(-1)	
			
def listePositions(mot,lettre):
	"""
		Renvoie les rangs des lettres (en comptant à partir de la droite)
		de mot qui sont égales à lettre.
	"""
	positions=[]
	for i in range(len(mot)):
		if mot[-(i+1)]==lettre:
			positions.append(i+1)
	return(positions)

####Calcul des mutations à effectuer

def suiteV(W,v,repw):
	"""
		Retourne la suite des v_(k) comme dans le [4.8.3 de Cluster
		structures on strata of flag varieties] étant donnés W un groupe
		de Weyl, repw une écriture réduite donnée d'un mot de W (vu comme
		liste d'entiers) et v un mot.
		
		Définir le groupe de Weyl par la syntaxe :
		W=WeylGroup('NOMDUGROUPE',prefix='s')
	"""
	n=len(W.simple_reflections())
	S=['s%s' %i for i in range(1,n+1)]
	S=W.simple_reflections() #Ainsi on peut manipuler les générateurs du
	#groupe de façon satisfaisante
	V=[W.unit()]#V sera la liste de nos v_(i) par défaut on a déjà v_(0)
	longueurW=len(repw)
	for i in range(1,longueurW+1):
		if (v*V[-1]**(-1)*S[repw[-i]]).length()<(v*V[-1]**(-1)).length():
			V=V+[S[repw[-i]]*V[-1]]
		else:
			V=V+[V[-1]]
	return(V)
	
def J(W,v,repw):
	"""
		Retourne l'ensemble des indices i de la liste obtenue par suiteV 
		tels que v_(i-1)=v_(i)
	"""
	L=suiteV(W,v,repw)
	n=len(L)
	res=[]
	for i in range (1,n):
		if L[i]==L[i-1]:
			res=res+[i]
	return(res)

def Jbarre(W,v,repw):
	"""
		Retourne l'ensemble des indices i de la liste obtenue par suiteV 
		tels que v_(i-1)!=v_(i)
	"""
	L=suiteV(W,v,repw)
	n=len(L)
	res=[]
	for i in range (1,n):
		if L[i]!=L[i-1]:
			res=res+[i]
	return(res)
	
def listeMuta(W,v,repw):
	"""
		Retourne la liste des mutations à faire en partant d'une graine de
		C_w pour arriver à une graine contenant une sous-graine qui soit 
		graine de C_{v,w}
	"""
	suiteJBarre=Jbarre(W,v,repw)
	listeMutations=[]
	listeSommetsASupprimer=[]
	listePos=[]
	listePosCourte=[]
	nbARetirer=0
	nbProjRetires=0
	for i in suiteJBarre:
		nbARetirer=0
		nbProjRetires=1
		#On réinitialise les comptes de mutations à ne pas prendre
		#en compte dans le calcul
		listePos=listePositions(repw,repw[-i])#On récupère toutes les emplacements
		#des lettres identiques à celle concernée (en fait tous les sommets
		#sur la même ligne
		listePosCourte=listePositions(repw[-i:],repw[-i]) #On prend la sous-liste
		#allant jusqu'au terme considéré actuellement
		for k in listePosCourte:#On regarde combien de mutations exclure dans celles qui précèdent
			if not(k in suiteJBarre):
				nbARetirer=nbARetirer+1
		#On regarde combien de mutations exclure car correspondant à des projectifs retirés
		for k in suiteJBarre:
			if k<i and repw[-k]==repw[-i]:
				nbProjRetires=nbProjRetires+1
		listeSommetsASupprimer=[listePos[-nbProjRetires]]+listeSommetsASupprimer
		#Le premier sommet qu'on retire est le projectif à cette étape
		listePos=listePos[nbARetirer:-nbProjRetires] #On retire les mutations
		#inutiles et le projectif
		listePos.reverse()#On la remet dans le sens de la notation
		#fonctionnelle
		listeMutations=listePos+listeMutations
	listeSommetsASupprimer.sort()
	return([listeMutations,listeSommetsASupprimer])
	
def listeMuta2(repw,v):
	"""
		Retourne la liste des mutations à faire en partant d'une graine de 
		C_w pour arriver à une graine contenant une sous graine qui soit 
		graine de C_{v,w}.
		On codera chaque mutation sous la forme (i,k) avec i l'indice sur
		lequel appliquer le sommet et k un entier qui vaut 0 si on mute et
		-1 si on supprime le sommet en question.
	"""
	#On est obligés de suivre la même méthode que pour listeMuta pour calculer la
	#liste des mutations car l'autre méthode exige d'appliquer les mutations en 
	#cours de route
	W=v.parent()
	suiteJBarre=Jbarre(W,v,repw)
	listeResultat=[]
	listeSommetsASupprimer=[]
	listePos=[]
	listePosCourte=[]
	nbARetirer=0
	nbProjRetires=0
	for i in suiteJBarre:
		nbARetirer=0
		nbProjRetires=1
		#On réinitialise les comptes de mutations à ne pas prendre
		#en compte dans le calcul
		listePos=listePositions(repw,repw[-i])#On récupère toutes les emplacements
		#des lettres identiques à celle concernée (en fait tous les sommets
		#sur la même ligne)
		listePosCourte=listePositions(repw[-i:],repw[-i]) #On prend la sous-liste
		#allant jusqu'au terme considéré actuellement
		for k in listePosCourte:#On regarde combien de mutations exclure dans celles qui précèdent
			if not(k in suiteJBarre):
				nbARetirer=nbARetirer+1
		#On regarde combien de mutations exclure car correspondant à des projectifs retirés
		for k in suiteJBarre:
			if k<i and repw[-k]==repw[-i]:
				nbProjRetires=nbProjRetires+1
		listeSommetsASupprimer=[listePos[-nbProjRetires]]+listeSommetsASupprimer
		#Le premier sommet qu'on retire est le projectif à cette étape
		if nbProjRetires==1:
			listePos=listePos[nbARetirer:]
		else:
			listePos=listePos[nbARetirer:-nbProjRetires+1] #On retire les mutations
		#inutiles et on laisse le projectif
		#On insère les indices sous la bonne forme dans la listeResultat
		for i in range(len(listePos)-1):#On ajoute les sommets à muter
			listeResultat=[(listePos[i],0)]+listeResultat
		listeResultat=[(listePos[-1],-1)]+listeResultat#On ajoute le projectif à supprimer
	return(listeResultat)

#### Calcul effectif des mutations

def prodScal(di,dDelta):
	"""
		Calcul le produit scalaire comme dans la proposition 12.6 de Kac-Moody groups 
		and cluster algebras
	"""
	n=len(di)
	res=[0]*n #On crée une liste vide de la bonne taille
	for i in range(n):#i sera l'indice des coordonnées de di
		for j in range(n):
			res[j]=res[j]+di[i]*dDelta[i][j]
	return(res)

	
def compaProdScal(Q,dDelta,listedi,k):
	"""
		Faire la comparaison de la proposition 12.6 de Kac-Moody groups 
		and cluster algebras et retourne les indices sur lesquels sommer
		pour calculer la mutation dans la direction k.
		Q est un carquois, dDelta, la liste des vecteurs de dimension des Delta_i
		listedi, l'ensemble des Delta-vecteurs de la graine.
	"""
	sources=[]
	buts=[]
	n=len(dDelta)
	sommeSources=[0]*n
	sommeButs=[0]*n#On initialise les listes
	sommeSources2=0
	sommeButs2=0
	for i in Q[1]:
		if i[0]==k:#On récupère la liste des flèches partant de k
			buts=buts+[i[1]]
		if i[1]==k:#On récupère la liste des flèches arrivant à k
			sources=sources+[i[0]]
	for i in sources:
		sommeSources=addListe(sommeSources,prodScal(listedi[i-1],dDelta))
	for i in buts:
		sommeButs=addListe(sommeButs,prodScal(listedi[i-1],dDelta))
	sommeSources2=sum(i for i in sommeSources)
	sommeButs2=sum(i for i in sommeButs)#On calcule le vecteur dimension total
	if sommeSources2>sommeButs2:#On aurait pu comparer les listes pour l'ordre 
		#lexicographique mais prop 12.4 nous assure l'équivalence des deux
		#et je ne suis pas sûr de l'implémentation python de deglex
		return(sources)
	else:#L'égalité est impossible par prop. 12.4
		return(buts)

def mutaStratesDeltaVecteurs(Graine,dDelta,k):
	"""
		Mute la graine donnée dans la direction k. Les facteurs directs 
		du module de la graine sont représentés par leurs delta-vecteurs.
		La graine est une liste [Q,listedi] où Q est le carquois sous-jacent
		et listedi la liste des delta-vecteurs. dDelta est la liste des 
		dimensions des delta_i.
	"""
	[Q,listedi]=Graine
	nouvelleListedi=deepcopy(listedi)
	listeSommetsIntervenant=compaProdScal(Q,dDelta,listedi,k)#On récupère la liste des sommets à considérer
	dketoile=multiplieListe(-1,listedi[k-1])
	for i in listeSommetsIntervenant:
		dketoile=addListe(dketoile,listedi[i-1])
	nouvelleListedi[k-1]=dketoile
	M=matriceAdjacence(Q)#On mute aussi le carquois, en passant par la mutation de sa matrice d'adjacence
	M=mutationMatrice(M,k)
	nouveauQ=creationCarquois(M)
	return([nouveauQ,nouvelleListedi])
	
def mutaStratesListeDeltaVecteurs(GraineInitiale,dDelta,listeMutations):
	"""
		Mute la graine donnée dans les directions données par listeMutations 
		(lue de droite à gauche). 
		Les facteurs directs du module de la graine sont représentés par 
		leurs delta-vecteurs. La graine est une liste [Q,listedi] où Q est 
		le carquois sous-jacent	et listedi la liste des delta-vecteurs. 
		dDelta est la liste des	dimensions des delta_i.
	"""
	GraineActuelle=deepcopy(GraineInitiale)
	n=len(listeMutations)
	for i in range(1,n+1):
		GraineActuelle=mutaStratesDeltaVecteurs(GraineActuelle,dDelta,listeMutations[-i])
	return(GraineActuelle)
	
#### Générateurs de la graine 	
	
def genFlechesCarquois(repw,M):
	"""
		Génère la liste des flèches représentant les flèches pour la 
		représentation du carquois de la graine initiale associée 
		au représentant du mot w (liste d'entiers). M est la matrice 
		d'adjacence du diagramme de Coxeter associé au groupe de Weyl.
	"""
	Q1=[]
	n=len(repw)
	iplus=0
	j=0
	jplus=0
	for i in range(1,n+1):#On met d'abord les flèches horizontales
		iplus=kplus(repw,i)
		if iplus!=n+1:
			Q1=Q1+[(i,iplus)]
	for i in range(1,n+1):#On rajoute les flèches ordinaires
		for j in range(i+1,n+1):
			if repw[-i]!=repw[-j]:
				iplus=kplus(repw,i)
				jplus=kplus(repw,j)
				if jplus>=iplus and iplus>j and j>i:
					for a in range(M[repw[-i]-1,repw[-j]-1]):
						Q1=Q1+[(j,i)]				
	return (Q1)
	
def graineInitiale(repw,W,type=0,affiche=False):
	"""
		Affiche la graine initiale associée au représentant du mot w, pour
		w dans un groupe de Weyl W dont le diagramme de Dynkin associé a
		pour matrice d'adjacence M.
		En type 0, les étiquettes sont des V_i, en type 1, les Delta-vecteurs
		en type 2, les g-vecteurs. Le type 2 ne fonctionne pas à ce jour.
		Le type 1 est difficilement lisible.
		Si affiche est à True, la graine sera représentée
	"""
	M=2*identity_matrix(W.cartan_type()[1])-CartanMatrix(W)
	n=len(repw)
	Q0=range(1,n+1)
	Q1=genFlechesCarquois(repw,M)
	Q=[Q0,Q1]
	coords=genCoordsCarquois(repw)
	if type==0:
		eti=['V%i' %s for s in range(1,n+1)]
	if type==1:
		eti=genEtiquettesCarquois(repw)
	if type==2:
		eti=gvecteursInitiaux(n)
	if affiche:
		if type==2:
			genCarquoisGvecteurs(Q,eti,repw,affiche=True)
		else :
			afficheCarquois(Q,coords,eti)
			#genCarquois2([Q,eti],[0]*n,[0]*n,repw,"Graine Initiale pour le representant"+str(repw)).show()#Ne fonctionne pas
	return([Q,eti])
	
def genEtiquettesCarquois(repw):
	"""		
		Génère les étiquettes correspondant aux coordonnées des Delta-vecteurs
		pour la représentation du carquois de la graine initiale associée 
		au représentant du mot w (liste d'entiers).
	"""
	l=[]
	n=len(repw)
	tmp=[]
	for i in range(1,n+1):
		tmp=[0]*n
		for j in range (1,i+1):
			if repw[-i]==repw[-j]:
				tmp[j-1]=1
		l=l+[tmp]
	return(l)		
	

def listeSommetsDansLaCategorie(listeDeltaV,v):
	"""
		Permet de savoir quels sommets sont dans la catégorie C^v en se 
		basant sur ses Delta_v-vecteurs indiqués dans la listeDeltaV
		Retourne la liste des indices des sommets (de 1 à n) concernés.
	"""
	n=len(listeDeltaV)
	resultat=[]
	l=v.length()
	for i in range(n):
		test=True#On change le booléen si on trouve une coordonnée non nulle
		j=0
		while test and j<l:
			if listeDeltaV[i][j]!=0:
				test=False
			j=j+1#Sinon on incrémente j
		if test:#Si toutes les coordonnées sont égales à 0
			resultat=resultat+[i+1]
	return(resultat)
	

#### Fonctions auxilliaires sur des listes

def addListe(l1,l2):
	"""
		Fait la somme terme à terme des deux listes
	"""
	n=len(l1)
	res=[0]*n
	for i in range(n):
		res[i]=l1[i]+l2[i]
	return(res)
	
def soustraitListe(l1,l2):
	"""
		Fait la différence terme à terme des deux listes : l1-l2
	"""
	n=len(l1)
	res=[0]*n
	for i in range(n):
		res[i]=l1[i]-l2[i]
	return(res)
	
def multiplieListe(k,l1):
	"""
		Multiplie tous les termes d'une liste l1 par k
	"""
	l2=copy(l1)
	for i in range(len(l1)):
		l2[i]=k*l2[i]
	return(l2)

