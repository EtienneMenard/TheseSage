# coding: utf8

def fonctionCoucoucvecteurs():
	print("Fichier cvecteurs.sage chargé")
	
fonctionCoucoucvecteurs()

#Ce fichier est une amélioration de g-vecteurs afin de permettre une bonne 
#prise en charge des colorations de sommets au sens de Keller (voir Demonet Keller '19 par exemple)
#Attention, comme les conventions de notations du carquois sont inversées par rapport aux 
#conventions habituelles, on considère comme vert un sommet qui n'est que but de flèches
#provenant de sommets gelés.

def carquoisEncadreInitial(repw,W,affiche=False):
	"""
		Version de graineInitiale compatible avec la coloration des sommets
		Affiche = Vrai pour afficher le carquois obtenu
		Le carquois sera représenté par une liste de sommets (positifs pour les sommets
		usuels, négatifs pour les sommets gelés) et une liste de flèches.
	"""
	M=2*identity_matrix(W.cartan_type()[1])-CartanMatrix(W)
	n=len(repw)
	Q0=list(range(1,n+1))#On génère la liste des sommets classiques
	Q0=Q0+list(range(-1,-n-1,-1))#Puis celle des sommets gelés
	Q1=genFlechesCarquois(repw,M)#On génère la liste des flèches classiques
	for i in range(1,n+1):
		Q1=Q1+[(-i,i)]#On rajoute les flèches initiales
	Q=[Q0,Q1]
	if affiche:
		graphe=genCarquoisCVecteurs(Q,repw)
	return(Q)
		
		
def genCoordsCarquoisCVecteurs(repw):
	"""
		Génère les coordonnées des sommets d'un carquois coloré (y compris
		celles des sommets gelés)
	"""
	coords=[]
	n=len(repw)
	for i in range(1,n+1):#Génération des coordonnées des sommets classiques
		coords=coords+[[-i,-repw[-i]]]
	for i in range(1,n+1):#Génération des coordonnées des sommets gelés
		coords=coords+[[-i,-repw[-i]-0.5]]
	return coords

def determinationCouleurSommets(Q):
	"""
		Renvoie la liste des couleurs des sommets de Q codé de la façon suivante:
		1 : vert, -1 : rouge, 0 : gelé (donc cyan)
	"""
	L=[0]*len(Q[0])
	for i in range(len(Q[0])):
		if Q[0][i]<0:#C'est un sommet gelé
			L[i]=0
		else : #Il faut déterminer sa couleur
			sommetCourant=Q[0][i]
			couleur=0 #On part d'une couleur indéterminée et on va regarder toutes les flèches
			for j in range(len(Q[1])):
				if (Q[1][j][1]==sommetCourant and Q[1][j][0]<0):#C'est une flèche verte
					if couleur==0:
						couleur=1
					elif couleur==-1:#Le sommet était rouge : erreur
						print("Erreur sur la flèche ",Q[1][j],"le sommet ",sommetCourant," était vert et la flèche le rend rouge")
						return False
					#Sinon on conserve la couleur 1
				elif (Q[1][j][0]==sommetCourant and Q[1][j][1]<0):#C'est une flèche rouge
					if couleur==0:
						couleur=-1
					elif couleur==1:#Le sommet était vert : erreur
						print("Erreur sur la flèche ",Q[1][j],"le sommet ",sommetCourant," était rouge et la flèche le rend vert")
						return False
			L[i]=couleur
	return(L)
					
					

def genCarquoisCVecteurs(Q,repw,affiche=True):
	"""
		Génère une image du carquois Q=[Q0,Q1] avec les sommets Q0 aux coordonnées
		calculées à partir de repw.
		Si affiche=True : ouvre une fenêtre pour l'afficher
	"""
	n=len(repw)
	[Q0,Q1]=Q
	carquois=Graphics()
	figcoords=Graphics()
	coords=genCoordsCarquoisCVecteurs(repw)
	[xmin,xmax,ymin,ymax,pas]=calculeDimensionsGraphiqueCarquois(coords)
	xmoy=(xmin+xmax)/2
	coords2=decalleCoord(coords,[0,5*pas])
	listeCouleurs=determinationCouleurSommets(Q)
	if listeCouleurs==False:
		print("Coloriage non consistant")
		return False
	for i in range(len(Q1)):#Création des flèches du carquois
		if (Q1[i][0]>0 and Q1[i][1]>0):#C'est une flèche ordinaire
			carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[Q1[i][1]-1],rgbcolor=(0,0.25,0.5),arrowsize=10)
		elif (Q1[i][0]<0 and Q1[i][1]>0):#C'est une flèche verte
			carquois=carquois+arrow2d(coords[n+abs(Q1[i][0])-1],coords[Q1[i][1]-1],rgbcolor=(0,0.5,0),arrowsize=5)
			#On retouche les coordonnées pour que ce soit correct
		elif (Q1[i][0]>0 and Q1[i][1]<0):#C'est une flèche rouge
			carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[n+abs(Q1[i][1])-1],rgbcolor=(0.9,0,0),arrowsize=5)
			#On retouche les coordonnées pour que ce soit correct
	for i in range(len(Q0)):#Création des sommets (par dessus les flèches à cause du chevauchement)
		if listeCouleurs[i]==1:#C'est un sommet vert
			carquois=carquois+point(coords[i],size=100,rgbcolor=(0,0.5,0))#On dessine le sommet
			carquois=carquois+text('V'+str(Q0[i]),coords2[i],rgbcolor=(0,0.5,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
			figcoords=figcoords+text('V'+str(Q0[i]),[xmoy,ymin+6*(len(Q0)/2-i)*pas],rgbcolor=(0,0.5,0),horizontal_alignment='center')#On le reporte sur l'autre graphe
		elif listeCouleurs[i]==-1:#C'est un sommet rouge
			carquois=carquois+point(coords[i],size=100,rgbcolor=(0.9,0,0))#On dessine le sommet
			carquois=carquois+text('V'+str(Q0[i]),coords2[i],rgbcolor=(0.9,0,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
			figcoords=figcoords+text('V'+str(Q0[i]),[xmoy,ymin+6*(len(Q0)/2-i)*pas],rgbcolor=(0.9,0,0),horizontal_alignment='center')#On le reporte sur l'autre graphe
		else:#On est sur un sommet gelé
			carquois=carquois+point(coords[i],size=100,rgbcolor=(0,0.7,0.7))#On dessine le sommet en cyan
			carquois=carquois+text("V'"+str(abs(Q0[i])),coords2[i],rgbcolor=(0,0.7,0.7),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet en cyan
	listeAreteFormatee=''
	for i in range(len(Q1)):
		listeAreteFormatee=listeAreteFormatee+str(Q1[i])+' , '
		if i%10==9:#On fait un saut de ligne toutes les 10 arêtes
			listeAreteFormatee=listeAreteFormatee+"\n"
	figcoords=figcoords+text("liste des aretes : \n"+listeAreteFormatee,[xmoy,ymin],horizontal_alignment='center',vertical_alignment='top')
	carquois.axes(False)
	figcoords.axes(False)
	figcoords.axes_range(xmin,xmax,ymin,ymin+3*(len(Q0)+2)*pas)
	carquois.axes_range(xmin,xmax+3*pas,ymin,ymax)
	if affiche:
		carquois.show()
		figcoords.show()
	return([carquois,figcoords])
	
#Reste à faire : évolution mutation

def matriceAdjacenceCVecteur(Q):
	"""
		Crée la matrice d'adjacence d'un carquois coloré
	"""
	[Q0,Q1]=Q
	nombreSommets=len(Q0)
	n=nombreSommets/2
	matrice=zero_matrix(nombreSommets,n)
	for i in Q1:
		if i[0]<0:
			if i[1]>0:
				#Si on a une flèche partant d'un sommet gelé, on gère 
				#pour le mettre au bon endroit dans la matrice
				matrice[abs(i[0])+n-1,i[1]-1]+=1
			#Si on a une flèche entre deux sommets gelés, on l'ignore
		else:
			if i[1]<0:#Si la flèche pointe vers un sommet gelé
				matrice[abs(i[1])-1+n,i[0]-1]-=1#On ne la voit que par antisymétrie de la partie principale
			else:#On est dans le cas classique 
				matrice[i[0]-1,i[1]-1]+=1
				matrice[i[1]-1,i[0]-1]-=1
	return(matrice)
				
def carquoisMatriceCVecteurs(M):
	"""
		Crée un carquois à partir de la matrice d'adjacence d'un carquois coloré
	"""
	[m,n]=M.dimensions()
	Q0=list(range(1,n+1))+list(range(-1,-n-1,-1))
	Q1=[]
	for i in range(n):
		for j in range(n):#On parcourt toutes les valeurs de la partie principales
			if M[i,j]>0:
				for k in range(M[i,j]):
					Q1=Q1+[(i+1,j+1)]
	for i in range(n,m):#On gère les sommets gelés
		for j in range(n):#On parcourt toutes les valeurs de la moitié basse
			#de la matrice
			if M[i,j]<0:#Ce sont des flèches rouges
				for k in range(-M[i,j]):
					Q1=Q1+[(j+1,-(i-n+1))]
			else:
				for k in range(M[i,j]):
					Q1=Q1+[(-(i-n+1),j+1)]
	return([Q0,Q1])
				

def deuxMouvementCvecteurs(valeur,mouvement):
	"""
		Effectue l'échange k<->k+1 tout en laissant les valeurs négatives fixes
	"""
	if valeur==mouvement:
		return(mouvement+1)
	elif valeur==mouvement+1:
		return(mouvement)
	else:#Ce n'est pas l'une des valeurs modifiées
		return(valeur)
	
		
def deuxMouvementCarquoisCvecteurs(Q,k):
	"""
		Modifie le carquois en accord avec un 2-mouvement en position (k+1,k)
		(lu à partir de la droite)
	"""
	#En fait on va juste changer les coordonnées des flèches.
	R0=deepcopy(Q[0])
	R=[]
	for i in Q[1]:
		R=R+[(deuxMouvementCvecteurs(i[0],k),deuxMouvementCvecteurs(i[1],k))]#On le rajoute au début pour ne pas repasser dessus
	return([R0,R])
	
def troisMouvementCarquoisCvecteurs(Q,k):
	"""
		Modifie le carquois en accord avec un 3-mouvement en position (k+2,k+1,k)
		(lu à partir de la droite)
	"""
	#On commence par muter le carquois dans la direction k
	M=matriceAdjacenceCVecteur(Q)
	M=mutationMatrice(M,k)
	R=carquoisMatriceCVecteurs(M)
	#Ensuite on permute k+1 et k+2 comme si c'était un 2-mouvement
	R=deuxMouvementCarquoisCvecteurs(R,k+1)
	return(R)
				

def appliquerListeMouvementsSSVZCarquoisCvecteurs(Q,liste):
	"""
		Applique la suite de mouvements SSVZ de liste au carquois Q
	"""
	R=deepcopy(Q)
	for i in liste:
		if len(i)==2:
			R=deuxMouvementCarquoisCvecteurs(R,i[0])
		else:#3 mouvement
			R=troisMouvementCarquoisCvecteurs(R,i[0])
	return(R)

def muterCarquoisCVecteur(Q,liste):
	"""
		Mute le carquois coloré successivement dans les directions de 
		liste (de gauche à droite)
	"""
	R=deepcopy(Q)
	M=matriceAdjacenceCVecteur(R)
	for i in liste:
		M=mutationMatrice(M,i)
	return(carquoisMatriceCVecteurs(M))
