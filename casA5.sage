# coding: utf8

#W=WeylGroup('A5',prefix='s')
#attach("~/Sage/run.sage")
#[s1,s2,s3,s4,s5]=W.simple_reflections()
#dimVi=[[1,0,0,0,0],[1,1,0,0,0],[1,1,1,0,0],[1,1,1,1,0],[1,1,1,1,1],[1,1,1,1,0],[1,1,1,2,1],[1,1,1,1,0],[1,1,2,2,1],[1,1,1,1,0],[1,2,2,2,1]]
#w=[2,1,3,2,4,3,5,4,3,2,1]
#v=s1*s3*s4
#dimMi=calculDimMi(dimVi,w)
#M=matrix([[0,1,0,0,0],[1,0,1,0,0],[0,1,0,1,0],[0,0,1,0,1],[0,0,0,1,0]])
#Graine=graineInitiale(w,W,1)
#Q=[range(1,6),[(1,2),(2,3),(3,4),(4,5)]]
#dDelta=vecteurdDelta(dimMi,Q,w)
#mutaAFaire=listeMuta(W,v,w)
#grainemutee=mutaStratesListeDeltaVecteurs(Graine,dDelta,[4,1])     
#afficheCarquois(grainemutee[0],genCoordsCarquois(w),grainemutee[1])
#(CartanMatrix(W.cartan_type())).dynkin_diagram()
