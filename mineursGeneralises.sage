# coding: utf8

def fonctionCoucouMineursGeneralises():
    print("Fichier mineursGeneralises chargé")
    
fonctionCoucouMineursGeneralises()

def decompositionGaussienne(M):
    """
        Retourne, si possible, la décomposition LDU de la matrice M où 
        L est une matrice unitriangulaire inférieure, D une matrice diagonale
        et U une matrice unitriangulaire supérieure
    """
    [n,m]=M.dimensions()
    if n!=m:
        print("Matrice non carrée")
        return False
    [P,L,U]=M.LU()
    if P!=identity_matrix(n):
        print("Décomposition impossible")
        return(False)
    D=diagonal_matrix(U.diagonal())
    U=D**-1*U
    return([L,D,U])

def calculSousEspaceDense(repw,W):
    """
        Calcule un sous espace dense de l'anneau des coordonnées sur le
        sous groupe unitrangulaire associé à repw
    """
    n=len(repw)
    [typeCartan,r]=W.cartan_type()
    listeFonctions=[]
    listeVariables=[]
    N=identity_matrix(r+1)
    Id=identity_matrix(r+1)
    if typeCartan=='A':
        for i in range(r):#Création des fonctions génératrices
            M=zero_matrix(r+1)
            M[i,i+1]=1
            listeFonctions=listeFonctions+[M]
        listeVariables=list(var(["t"+"%02i" %k for k in range (n,0,-1)]))
        for i in range(1,n+1):
            N=(Id+listeFonctions[repw[-i]-1]*listeVariables[-i])*N
    else:
        print("Type non géré")
        return False
    return(N.expand())
    
def calculMineur(M,l):
    """
        Calcule le mineur de la matrice donnée par les lignes et les colonnes
        l=[listeLignes,listeColonnes]
    """
    [listeLignes,listeColonnes]=l
    lignesFinales=[]
    colonnesFinales=[]
    if (len(listeLignes)!=len(listeColonnes)) or (M.nrows()!=M.ncols()):
        print("Matrice non carrée")
        return(False)
    lignes=M.rows()
    for i in listeLignes:
        lignesFinales=lignesFinales+[lignes[i-1]]
    M2=matrix(lignesFinales)
    colonnes=M2.columns()
    for i in listeColonnes:
        colonnesFinales=colonnesFinales+[colonnes[i-1]]
    M3=matrix(colonnesFinales)
    M3=M3.transpose()
    return(M3.det())
    
def calculPoids(indicePoidsFondamental,repw,W):
    """
        Calcule, dans An, l'action d'un produit des réflections de représentant
        repw sur le poids fondamental ϖ_i
    """
    if W.cartan_type()[0]!='A':
        print("Type non pris en charge")
        return(False)
    S=set(range(1,indicePoidsFondamental+1))
    n=len(repw)
    for i in range(n):
        S=actionTranspo(S,repw[-i-1])
    return(S)

def actionTranspo(S,k):
    """
       Calcule l'action de la transposition (k,k+1) sur l'ensemble S 
    """
    newS=[]
    for i in S:
        if i==k:
            newS=newS+[k+1]
        elif i==k+1:
            newS=newS+[k]
        else:
            newS=newS+[i]
    return(set(newS))
