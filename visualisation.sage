# coding: utf8

def fonctionCoucouVisualisation():
	print ("Fichier visualisation.sage chargé")
	
fonctionCoucouVisualisation()


def afficheCarquois(Q,coords,eti=[],dim=-1):
	"""
		Affiche le carquois Q=[Q0,Q1] avec les sommets Q0 aux coordonnées
		indiquées par la liste coords et les étiquettes indiquées par la liste
		eti. Si les dimensions [xmin,xmax,ymin,ymax,pas] ne sont pas donnés
		ils sont calculés automatiquement par 
		calculeDimensionsGraphiqueCarquois.
	"""
	[Q0,Q1]=Q
	carquois=Graphics()
	if eti==[]:
		eti=range(1,len(Q0)+1)
	if dim==-1:
		[xmin,xmax,ymin,ymax,pas]=calculeDimensionsGraphiqueCarquois(coords)
	else:
		[xmin,xmax,ymin,ymax,pas]=dim
	coords2=decalleCoord(coords,[0,5*pas])
	for i in range(len(Q1)):#Création des flèches
		carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[Q1[i][1]-1],rgbcolor=(0,0.25,0.5))
	for i in range(len(eti)):#Création des sommets (par dessus les flèches à cause du chevauchement)
		carquois=carquois+text(eti[i],coords2[i],rgbcolor=(0,0,0),background_color='white')+point(coords[i],size=100,rgbcolor=(1,0,0))
		carquois=carquois+text(str(i+1)+" : "+str(eti[i]),[xmin,ymin+3*i*pas],rgbcolor=(0,0,0))
	carquois.axes(False)
	carquois.axes_range(xmin,xmax,ymin,ymax)
	carquois.show()

def decalleCoord(coords,listepas):
	"""
		Décale les coords (liste des listes à 2 éléments) de listepas=[xpas,ypas] :
		'xpas' vers le haut et de 'ypas' vers la droite
	"""
	[xpas,ypas]=listepas
	res=deepcopy(coords)#Sinon ça ne copie que les pointeurs des sous-listes
	if xpas!=0:
		for i in range(len(res)):
			res[i][0]=res[i][0]+xpas
	if ypas!=0:
		for i in range(len(res)):
			res[i][1]=res[i][1]+ypas
	return res

def calculeDimensionsGraphiqueCarquois(coords):
	n=len(coords)
	xmin=coords[0][0]
	xmax=coords[0][0]
	ymin=coords[0][1]
	ymax=coords[0][1]
	pas=0
	pasBord=0
	for i in range(1,n):
		if coords[i][0]>xmax:
			xmax=coords[i][0]
		if coords[i][0]<xmin:
			xmin=coords[i][0]
		if coords[i][1]>ymax:
			ymax=coords[i][1]
		if coords[i][1]<ymin:
			ymin=coords[i][1]
	if n!=0:
		pasBord=1/(2*n)*max(xmax-xmin,ymax-ymin)
	xmax=xmax+pasBord
	xmin=xmin-pasBord
	ymax=ymax+pasBord
	ymin=ymin-pasBord
	pas=pasBord/10
	return([xmin,xmax,ymin,ymax,pas])
	
def genCoordsCarquois(repw):
	"""
		Génère les coordonnées pour la représentation du carquois de la 
		graine initiale associée au représentant du mot repw (liste d'entiers).
	"""
	coords=[]
	n=len(repw)
	for i in range(1,n+1):
		coords=coords+[[-i,-repw[-i]]]
	return coords
	
def convertirCoordonnees(liste,lettre='e'):
	"""
		Transforme une liste de coordonnées représentée de façon dense
		en représentation lacunaire (sous forme de somme de vecteurs
		canoniques). Prend en entrée la liste à générer et une lettre et 
		renvoie une expression symbolique dont les vecteurs canoniques 
		sont notés par lettre et un indice
	"""
	n=len(liste)+1#On récupère la dimension du vecteur dimension
	if n<=10:
		tupleVariables=var([lettre+'%i' %i for i in range(1,n)])
	else:
		tupleVariables=var([lettre+'%02i' %i for i in range(1,n)])
	#On génère n variables ei pour chacun des vecteurs canoniques
	tmp=0
	for i in range(len(liste)):
		tmp=tmp+liste[i]*tupleVariables[i]
	return tmp
	

def genCarquois2(Q,listeDeltaV,listeDeltaW,repw,listeDimensionDeltaWVecteurs=[],texte="",exclusion=[],lettreW='e',lettreV='f',couperADroite=False,v=[]):
	"""
		Affiche le carquois Q=[Q0,Q1] avec les sommets Q0 aux coordonnées
		calculées à partir de repw et les étiquettes indiquées par listeDeltaV
		et listeDeltaW.
		Sur le graphique est écrit la chaine de caractères texte passée en paramètre.
		La liste exclusion permet de choisir des sommets qu'on n'affichera pas.
		Le booléen couperADroite permet de choisir si on affiche ou pas les modules
		déjà dans la bonne catégorie. Si il vaut True, il faut lui passer v, élément 
		du groupe de Weyl.
	"""
	[Q0,Q1]=Q
	if  listeDimensionDeltaWVecteurs!=[]:
		listeProjectifs=detecterProjectifs(Q0,Q1,listeDeltaW,listeDimensionDeltaWVecteurs,exclusion)
	else:
		listeProjectifs=[]
	carquois=Graphics()
	coords=genCoordsCarquois(repw)
	[xmin,xmax,ymin,ymax,pas]=calculeDimensionsGraphiqueCarquois(coords)
	xmoy=(xmin+xmax)/2
	coords2=decalleCoord(coords,[0,5*pas])
	dimStratesDeltaW=calculDimMi2(W,completeMot(repw,W))		#On récupère les dimensions des vecteurs M_{i,\bar{w}} pour déterminer les projectifs
	if couperADroite:
		if v!=[]:
			listeSommetsACouper=listeSommetsDansLaCategorie(listeDeltaV,v)
		else:
			print("v n'a pas été défini")
			return False
	figcoords=Graphics()
	figcoords=figcoords+text("Delta_v vecteurs",[xmoy-pas,ymin+3*(len(Q0)+2)*pas],rgbcolor=(1,0,0),horizontal_alignment='right')
	figcoords=figcoords+text("Delta_w vecteurs",[xmoy+pas,ymin+3*(len(Q0)+2)*pas],rgbcolor=(0,0.5,0),horizontal_alignment='left')
	if not couperADroite:
		for i in range(len(Q1)):#Création des flèches
			if not((Q1[i][0]in exclusion) or (Q1[i][1] in exclusion)):
				carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[Q1[i][1]-1],rgbcolor=(0,0.25,0.5),arrowsize=5)
		for i in range(len(Q0)):#Création des sommets (par dessus les flèches à cause du chevauchement)
			if i+1 in exclusion:#On représente différemment les points à exclure
				carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0,0,0),fontsize='x-large',horizontal_alignment='left')+point(coords[i],size=100,rgbcolor=(0,0,1))
				figcoords=figcoords+text('V'+str(i+1)+" : ",[xmoy,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0,0),horizontal_alignment='center')#Report du nom des sommets dans un coin
			else :
				if i+1 in listeProjectifs:#On représente différemment les projectifs
					carquois=carquois+point(coords[i],size=100,rgbcolor=(0.18,0.6,0.95))#On dessine le sommet
					carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0.18,0.6,0.95),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				else:
					carquois=carquois+point(coords[i],size=100,rgbcolor=(1,0,1))#On dessine le sommet
					carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0,0,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				figcoords=figcoords+text('V'+str(i+1)+" : "+"  "*(len(str(convertirCoordonnees(listeDeltaV[i],lettreV)))),[xmoy-pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0,0),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaV[i],lettreV)),[xmoy-3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(1,0,0),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaW[i],lettreW)),[xmoy+3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0.5,0),horizontal_alignment='left')
				#Report du nom des sommets dans un coin et écriture des delta_v et delta_w vecteurs
	else:#couperADroite est activé
		for i in range(len(Q1)):#Création des flèches
			if not((Q1[i][0]in exclusion) or (Q1[i][1] in exclusion) or (Q1[i][0]in listeSommetsACouper) or (Q1[i][1] in listeSommetsACouper)):
				carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[Q1[i][1]-1],rgbcolor=(0,0.25,0.5),arrowsize=5)
		for i in range(len(Q0)):#Création des sommets (par dessus les flèches à cause du chevauchement)
			if i+1 in exclusion:#On représente différemment les points à exclure
				carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0.3,0.3,0.3),fontsize='x-large',horizontal_alignment='left')+point(coords[i],size=100,rgbcolor=(0.3,0.3,0.3))
				figcoords=figcoords+text('V'+str(i+1)+" : ",[xmoy,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0.3,0.3,0.3),horizontal_alignment='center')#Report du nom des sommets dans un coin
			elif i+1 in listeSommetsACouper:#On représente différemment les modules déjà dans la catégorie
				carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0.97,0.83,0.24),fontsize='x-large',horizontal_alignment='left')+point(coords[i],size=100,rgbcolor=(0.97,0.83,0.24))
				figcoords=figcoords+text('V'+str(i+1)+" : "+"  "*(len(str(convertirCoordonnees(listeDeltaV[i],lettreV)))),[xmoy-pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0.97,0.83,0.24),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaV[i],lettreV)),[xmoy-3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0.97,0.83,0.24),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaW[i],lettreW)),[xmoy+3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0.5,0),horizontal_alignment='left')
			else :
				if i+1 in listeProjectifs:#On représente différemment les projectifs
					carquois=carquois+point(coords[i],size=100,rgbcolor=(0.18,0.6,0.95))#On dessine le sommet
					carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0.18,0.6,0.95),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				else:
					carquois=carquois+point(coords[i],size=100,rgbcolor=(1,0,1))#On dessine le sommet
					carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0,0,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				figcoords=figcoords+text('V'+str(i+1)+" : "+"  "*(len(str(convertirCoordonnees(listeDeltaV[i],lettreV)))),[xmoy-pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0,0),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaV[i],lettreV)),[xmoy-3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(1,0,0),horizontal_alignment='right')
				figcoords=figcoords+text(str(convertirCoordonnees(listeDeltaW[i],lettreW)),[xmoy+3*pas,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0.5,0),horizontal_alignment='left')
				#Report du nom des sommets dans un coin et écriture des delta_v et delta_w vecteurs
	listeAreteFormatee=''
	for i in range(len(Q1)):
		listeAreteFormatee=listeAreteFormatee+str(Q1[i])+' , '
		if i%10==9:#On fait un saut de ligne toutes les 10 arêtes
			listeAreteFormatee=listeAreteFormatee+"\n"
	figcoords=figcoords+text("liste des aretes : \n"+listeAreteFormatee,[xmoy,ymin],horizontal_alignment='center',vertical_alignment='top')
	carquois=carquois+text(texte,[(xmin+xmax)/2,ymin],rgbcolor=(1,0,0))
	carquois.axes(False)
	figcoords.axes(False)
	figcoords.axes_range(xmin,xmax,ymin,ymin+3*(len(Q0)+2)*pas)
	carquois.axes_range(xmin,xmax+3*pas,ymin,ymax)
	return([carquois,figcoords])

def detecterProjectifs(Q0,Q1,listeDeltaWVecteurs,listeDimensionDeltaWVecteurs,exclusion):
	"""
		Prend en entrée la liste des sommets et arêtes du carquois et la liste des
		sommets exclus et renvoie la liste des sommets projectifs
		Critère : inégalité des dimensions des modules adjacents 
	"""
	resultat=[]		#Liste des sommets projectifs
	n=len(listeDimensionDeltaWVecteurs[0]) #Astuce pour récupérer le nombre de sommets
	r=len(Q0)		#Nombre de sommets
	for i in range(r):#On va regarder pour chacun des sommets
		indiceCourant=Q0[i]
		if indiceCourant not in exclusion:
			dimBut=vector([0]*n)
			dimSource=vector([0]*n)
			for k in Q1:#On repère les sommets adjacents
				if (k[0]==indiceCourant) and (k[1]not in exclusion):
					for j in range(r):#On va parcourir chacune des coordonnées du DeltaVecteur
						coefficientDeltaVecteurCourant=listeDeltaWVecteurs[k[1]-1][j]
						if coefficientDeltaVecteurCourant!=0:
							dimBut=dimBut+coefficientDeltaVecteurCourant*vector(listeDimensionDeltaWVecteurs[j])
				elif (k[1]==indiceCourant) and (k[0]not in exclusion):
					for j in range(r):#On va parcourir chacune des coordonnées du DeltaVecteur
						coefficientDeltaVecteurCourant=listeDeltaWVecteurs[k[0]-1][j]
						if coefficientDeltaVecteurCourant!=0:
							dimSource=dimSource+coefficientDeltaVecteurCourant*vector(listeDimensionDeltaWVecteurs[j])
			#Une fois calculée la dimension de tous les vecteurs adjacents source et but, on compare ces deux vecteurs
			if dimSource!=dimBut:
				resultat=resultat+[Q0[i]]#Si on n'a pas égalité, le sommet est projectif
	return(resultat)
