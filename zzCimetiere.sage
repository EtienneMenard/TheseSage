#Recueil de toutes les fonctions abandonnées


def alpha(mot,lettre,rang):#Ne correspond à rien
	"""
		Retourne la rang ième fonction alpha associée à la lettre pour le mot.
	"""
	tlettre=compteLettre(mot,lettre)#Pour savoir si on a pas directement l'identité
	if rang>tlettre-1:#On ne compte pas le projectif à la fin de la ligne
		return([])
	alpha1=listePositions(mot,lettre)
	alpha1.reverse()#On remet les mutations dans le bon sens
	return(alpha1[rang:])#En renvoyant la liste tronquée de cette façon
	#on s'assure aussi de supprimer le dernier élément de la liste qui 
	#correspond à un projectif, donc non-mutable
		
def muTilde(motv,motw,indice):#Ne correspond à rien
	"""
		Retourne la composition des alpha traduisant les mutations 
		nécessaires sur la ligne correspond à motv[indice] (l'indice ième lettre)
		pour son occurrence numero indice dans v.
	"""
	lettre=motv[-indice]
	listePos=listePositions(motv,lettre)#On récupère toutes les positions
	#de la lettre dans le mot pour savoir combien d'alpha(lettre) on a 
	#déjà utilisé
	a=0
	for i in listePos:#On compte combien de fois on a déjà pris alpha
		if i<indice:
			a=a+i
	b=a+indice+1#Car range va ignorer le dernier
	listeMuta=[]
	for i in range(a+1,b):
		listeMuta=alpha(motw,lettre,i)+listeMuta
	return listeMuta

def muPoint(motv,motw):#Ne correspond à rien
	"""
		Définit l'ensemble des mutations à appliquer au carquois d'une
		graine de C_w pour obtenir une graine de C_v,w après ablation des
		projectifs
	"""
	k=len(motv)
	listeDesMutations=[]
	for i in range(k):
		print('on en est au rang',i+1,' la lettre est ',motv[-i-1] )
		listeDesMutations=muTilde(motv,motw,i+1)+listeDesMutations
	return listeDesMutations

def prodScalUsuel(d1,d2):
	"""
		Calcule le produit scalaire usuel de deux vecteurs
	"""
	res=0
	for i in range(len(d1)):
		res=res+d1[i]*d2[i]
	return(res)

def calculVecteurdDeltaV(v,w):
	"""
		Calcule la liste des vecteurs d_Delta_v par rapport au représentant
		repV (liste d'entiers) d'un mot v d'un groupe de Weyl W. On choisit
		un complété à gauche en w0 du représentant le plus à droite de v
		parmi les sous-mots de W.
	"""
	W=v.parent()#On récupère le groupe de Weyl sous-jacent
	repV=sousMotReduitMinimal(v,w)#On récupère le sous-mot le plus à droite
	repW0=completeMot(repV,W)#On le complète
	listeDimMi=calculDimMi2(W,repW0)#On calcule les vecteurs dimensions des
	#modules Mi associés à repW0
	Q=creerCarquois(W)#On a besoin d'un carquois du type de W
	listedDeltav=vecteurdDelta(listeDimMi,Q,repW0)
	return(listedDeltav)

def calculChangementBaseDeltaVecteur(EtiquettesDeltawVecteurs,dDeltaw,dDeltaw0complet):
	"""
		Change de base pour que des vecteurs exprimés dans une Delta-base
		relative à un w donné soient désormais exprimés dans une Delta-base
		relative à un représentant v0complet du mot de longueur maximale.
		Il n'est pas nécessaire que w soit un mot de longueur maximale, si
		nécessaire on le mettra sous la bonne forme en rajoutant à droite
		suffisamment de zeros
	"""
	n=len(dDeltaw0complet) #On récupère la dimension de l'espace vectoriel 
	#des DeltaW0complet vecteurs
	k=len(dDeltaw)#On récupère l'autre dimension, longueur de w
	listeEtiquetteRallongee=[[0]*n]*k#On initialise une liste vide qui va contenir les 
	#deltaWvecteurs rallongés
	vectwDeltaVecteurDansBaseBi=[vector([0]*n)]*k
	listewDeltaVecteurDansBaseBi=[[0]*n]*k
	vectdDeltawRallonge=[[0]*n]*k
	for i in range(k):
		listeEtiquetteRallongee[i]=EtiquettesDeltawVecteurs[i]+[0]*(n-k)
		vectdDeltawRallonge[i]=vector(dDeltaw[i]+[0]*(n-k))
	#On construit la matrice de passage de la base des Delta_w-vecteurs
	#vers la base de Bi. On construit en fait sa matrice transposée puis
	#on la renverse
	for i in range(k):#Pour chaque Delta-w-vecteur...
		for j in range(k):#On calcule ses coordonnées dans la base Bi
			#On fait attention à ne pas accéder à des dDelta_w vecteurs
			#qui n'existent pas
			vectwDeltaVecteurDansBaseBi[i]=vectwDeltaVecteurDansBaseBi[i]+EtiquettesDeltawVecteurs[i][j]*vectdDeltawRallonge[j]
	#Remettre sous forme de liste de liste
	for i in range(k):
		listewDeltaVecteurDansBaseBi[i]=list(vectwDeltaVecteurDansBaseBi[i])
	return(listewDeltaVecteurDansBaseBi)
