# coding: utf8

def fonctionCoucoutests():
	print ("Fichier tests.sage chargé")

fonctionCoucoutests()


[W,[s1,s2,s3,s4,s5,s6]]=creerGroupeWeyl('A6')
w=s1*s2*s3*s4*s5*s6*s2*s3*s4*s5*s3*s4*s1*s2*s3*s1*s2*s1
repW=motVersRep(w)
v=s1*s4*s3*s2*s3*s1*s2
v.bruhat_le(w)
repV=sousMotReduitMinimal(v,repW)
repWcomplete=completeMot(repW,W)
repVcomplete=completeMot(repV,W)
listeDimMiWcomplete=calculDimMi2(W,repWcomplete)
listeDimMiVcomplete=calculDimMi2(W,repVcomplete)
Q=creerCarquois(W)
dDeltaWcomplete=vecteurdDelta(listeDimMiWcomplete,Q,repWcomplete)
dDeltaVcomplete=vecteurdDelta(listeDimMiVcomplete,Q,repVcomplete)
EtiquettesDeltawVecteurs=genEtiquettesCarquois(repWcomplete)
calculChangementBaseDeltaVecteur(EtiquettesDeltawVecteurs,dDeltaWcomplete,dDeltaVcomplete)


[W,[s1,s2,s3,s4]]=creerGroupeWeyl('A4')
w=s1*s2*s3*s4*s2*s1
v=s2*s3*s4*s2
repV=sousMotReduitMinimal(v,repW)
repWcomplete=completeMot(repW,W)
repVcomplete=completeMot(repV,W)
listeDimMiWcomplete=calculDimMi2(W,repWcomplete)
listeDimMiVcomplete=calculDimMi2(W,repVcomplete)
Q=creerCarquois(W)
dDeltaWcomplete=vecteurdDelta(listeDimMiWcomplete,Q,repWcomplete)
dDeltaVcomplete=vecteurdDelta(listeDimMiVcomplete,Q,repVcomplete)
EtiquettesDeltawVecteurs=genEtiquettesCarquois(repWcomplete)

#gap('GetBraidRelations(WeylGroup(RootSystem("A",6)),[1,2,1],[2,1,2])')
