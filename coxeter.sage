# coding: utf8

def fonctionCoucouCoxeter():
	print ("Fichier coxeter.sage chargé")
	
fonctionCoucouCoxeter()

def deuxMouvement(rep,k):
	"""
		Prend en entrée la liste rep (liste d'entiers) et l'entier k. 
		Renvoie la liste rep avec les lettres en position
		k et k+1 échangées.
		Attention, les positions sont prises de la droite vers la gauche.
		Attention, ne vérifie pas la validité du mouvement.
	"""
	if k>len(rep)-1 or k<=0:
		print("Position invalide")
		return []
	res=deepcopy(rep)
	res[-k]=rep[-(k+1)]
	res[-(k+1)]=rep[-k]
	return(res)
	
def troisMouvement(rep,k):
	"""
		Prend en entrée la liste rep (liste d'entiers) et l'entier
		k. Renvoie la liste rep avec un mouvement de
		tresse (i_k+2,i_k+1,i_k)->(i_k+1,i_k,i_k+1).
		Attention, les positions sont prises de la droite vers la gauche.
		Attention, ne vérifie pas la validité du mouvement.
	"""
	if k>len(rep)-2 or k<=0:
		print("Position invalide")
		return[]
	res=deepcopy(rep)
	res[-k]=rep[-(k+1)]
	res[-(k+1)]=rep[-k]
	res[-(k+2)]=rep[-(k+1)]
	return(res)

def mouvementsPossibles(rep,W):
	"""
		Prend en entrée la liste rep (liste d'entiers) et le groupe de Weyl W
		et calcule l'ensemble des 2 et 3-mouvements possibles à partir 
		du représentant rep.
		Attention, ne fonctionne qu'avec des groupes de type A,D ou E.
		Attention positions numérotées de la droite à la gauche.
	"""
	listeMouvementsPossibles=[]
	[Q0,Q1]=creerCarquois(W)
	listeAretesNonOrientees=[]
	for i in Q1:
		listeAretesNonOrientees=listeAretesNonOrientees+[set(i)]
	#On convertit les tuples des arêtes en ensemble pour oublier l'ordre
	#comme dans le diagramme de Dynkin
	for i in range(1,len(rep)):
		if {rep[-i],rep[-(i+1)]} in listeAretesNonOrientees:
			if i<len(rep)-1 and rep[-(i+2)]==rep[-i] :
				listeMouvementsPossibles=listeMouvementsPossibles+[(i,i+1,i+2)]
		else:
			listeMouvementsPossibles=listeMouvementsPossibles+[(i,i+1)]
	return listeMouvementsPossibles
			
def modifAleatoireRep(rep,nbPas,W):
	"""
		Prend en entrée un représentant d'un mot, rep (liste d'entiers)
		 d'un groupe de Weyl W et applique nbPas 
		 2 et 3 mouvements (possiblement deux fois le même) pour donner
		 un autre representant et la liste des mouvements effectués.
	"""
	listeMouvements=[]
	mouvement=()
	tmp=deepcopy(rep)
	historique=[]
	for i in range(nbPas):
		listeMouvements=mouvementsPossibles(tmp,W)
		mouvement=choice(listeMouvements)#On prend un élément au hasard
		tmp=appliquerMouvement(tmp,mouvement)
		historique=historique+[mouvement]
	return([tmp,historique])
		
def appliquerMouvement(rep,mouvement):
	"""
		Prend en entrée le représentant d'un mot, rep (liste d'entiers)
		et le mouvement à lui faire faire (tuple des positions des lettres
		concernées)
		Renvoie le représentant modifié en accord.
		Ne fonctionne que pour des 2 et 3-mouvements
	"""
	if len(mouvement)==2:
			return(deuxMouvement(rep,mouvement[0]))
	if len(mouvement)==3:
			return(troisMouvement(rep,mouvement[0]))
			
def appliquerListeMouvements(rep,liste):
	"""
		Prend en entrée le représentant d'un mot, rep (liste d'entiers)
		et la liste des mouvements à lui faire faire (liste de tuple des 
		positions des lettres concernées).
		Renvoie le représentant modifié en accord.
		Ne fonctionne que pour des 2 et 3-mouvements
	"""
	repCourant=rep
	for i in liste:
		repCourant=appliquerMouvement(repCourant,i)
	return repCourant
	
def permutation(lettre,permutation):
	"""
		Prend en entrée une lettre (entier) et un uplet (permutation)
		et retourne la lettre modifiée selon la permutation
	"""
	curseur=0
	if lettre in permutation:
		while permutation[curseur]!=lettre:#On récupére l'indice du terme
		#de la permutation
			curseur=curseur+1
		if curseur==len(permutation)-1:
			return(permutation[0])
		return(permutation[curseur+1])
	return(lettre)

