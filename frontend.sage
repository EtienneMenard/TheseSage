# coding: utf8

def fonctionCoucoufrontend():
	print ("Fichier frontend.sage chargé")
	
fonctionCoucoufrontend()

def mutationPasAPas(W,v,repw):
	"""
		Calcule l'évolution de la graine initiale
		de C_w choisie en graine initiale de C_v,w en terme de Delta-vecteurs
		Nécessite W, le groupe de Weyl considére, v le mot dans W, 
		repw, le représentant du mot w
	"""
	if not(v.bruhat_le(repVersMot(repw,W))):
		print(u"v n'est pas plus petit que w pour l'ordre de Bruhat")
		return False
	M=2*identity_matrix(W.cartan_type()[1])-CartanMatrix(W)
	#On crée la matrice d'adjacence du diagramme de Dynkin à partir de la 
	#matrice de Cartan de W via des ajustements mineurs
	dimVi=calculDimVi2(W,repw)#Nécessite d'avoir chargé calculGroupeWeyl.sage
	[mutationsAFaire,sommetsAExclure]=listeMuta(W,v,repw)#On récupère la liste des mutations à faire
	#On génère la graine initiale
	graineIni=graineInitiale(repw,W,1,False)
	graineActuelle=deepcopy(graineIni)
	dDelta=vecteurdDelta(calculDimMi(dimVi,repw),creationCarquois2(M),repw)
	texte=u'(T)'
	listeCarquois=[Graphics()]*(len(mutationsAFaire)+1)
	listeCarquois[0]=genCarquois2(graineActuelle,repw,u"Mutation : "+texte)
	for i in range(1,len(mutationsAFaire)+1):#On affiche le résultat de chaque mutation
		graineActuelle=mutaStratesDeltaVecteurs(graineActuelle,dDelta,mutationsAFaire[-i])
		if i==1:
			texte=r"$\mu$"+str(mutationsAFaire[-i])+'(T)'
		else:
			texte=r'$\mu$'+str(mutationsAFaire[-i])+r'$\circ$'+texte
		listeCarquois[i]=genCarquois2(graineActuelle,repw,u"Mutation : "+texte)
	k=len(listeCarquois)
	nbligne=ceil(k/3)
	G=graphics_array(listeCarquois,nbligne,3)
	return([graineActuelle,G])	

def creerGroupeWeyl(nom):
	"""
		Permet de définir le groupe Weyl de type nom (chaine de caractères)
		et ses réflexions simples nommées s_i en fonction du diagramme de
		Dynkin affiché.
		Renvoie (W,s) avec W le groupe de Weyl considéré et s la liste
		des réflexions.
	"""
	type=nom[0]
	n=int(nom[1:])#On récupère tout à partir du deuxième caractère (et pas juste 
	#le deuxième, pour gérer le cas d'un nombre à plusieurs chiffres)
	W=WeylGroup(nom,prefix='s')
	s=list(var(['s%i' %i for i in range(1,n+1)]))
	print(DynkinDiagram(nom))
	for j in range(1,n+1):
		s[j-1]=(W.simple_reflections())[j]
	return(W,s)

def creerCarquois(W):
	"""
		Renvoie un carquois correspondant de même type de Dynkin que W, 
		avec les flèches choisies pour être toujours croissantes
	"""
	M=2*identity_matrix(W.cartan_type()[1])-CartanMatrix(W)#On récupère la
	#matrice d'adjacence (symétrique) convenable
	return(creationCarquois2(M))


def testAlgo(repw,v,mutationsAFaire=[],listeSommetsAExclure=[],lettrepourW='e',lettrepourV='f',enregistrement=False,enregistrementSSVZ=False):
	"""
		Prend en entrée le représentant repw d'un mot w d'un groupe de Weyl W
		un mot d'un groupe de Weyl v tel que v<w pour l'ordre de Bruhat dans W.
		Et retourne le carquois de la graine pour l'algèbre amassée de C_v,w
		calculée d'après l'algorithme de Bernard Leclerc où les sommets sont
		exprimés en fonction des Delta-vecteurs relatifs au complété de v.
		On pourra aussi spécifier une autre liste de mutations et de
		sommets à exclure pour tenter de modifier la conjecture ainsi qu'une
		lettre pour indexer les delta_w vecteurs et une pour indexer les
		delta_v vecteurs.
		L'option enregistrement prend en entrée un booléen et si celui ci
		vaut true, une copie de chacune des images sera enregistrée dans un 
		dossier indiqué.
		L'option enregistrementSSVZ prend en entrée un booléen et si celui ci
		vaut true, une copie de chacune des images du calcul des Delta-vecteurs
		sera enregistrée dans un dossier indiqué.
	""" 
	W=v.parent()
	if not(v.bruhat_le(repVersMot(repw,W))):#protection
		print(u"v n'est pas plus petit que w pour l'ordre de Bruhat")
		return False
	repvcomplete=completeMot(sousMotReduitMinimal(v,repw),W)
	repwcomplete=completeMot(repw,W)
	#Mouvements SSVZ
	listeMouvements=calculMouvementsTresses(repvcomplete,repwcomplete,W)
	[repwinutile,graineIni,dDeltaV]=muteCarquoisSSVZ(repvcomplete,listeMouvements,W,affiche=False,enregistrement=enregistrementSSVZ)[:3]
	#Initialisation des données
	listeDeltaV=graineIni[1]
	dDeltaW=vecteurdDelta(calculDimMi2(W,repwcomplete),creerCarquois(W),repwcomplete)
	graineActuelle=deepcopy(graineIni)
	texte=u'(T)'
	if mutationsAFaire==[]:
		[mutationsAFaire,listeSommetsAExclure]=listeMuta(W,v,repw)
	listeSommetsAExclure=listeSommetsAExclure+range(len(repw)+1,W.long_element().length()+1)
	listeCarquois=[Graphics()]*(2*(len(mutationsAFaire)+2))#On rajoute le carquois
	#avant mutation et celui après, une fois qu'on a enlevé les sommets
	#et on aura un graphique pour les images, un pour les coordonnées
	[Q,listeDeltaW]=graineInitiale(repwcomplete,W,1,False)
	[listeCarquois[0],listeCarquois[1]]=genCarquois(Q,listeDeltaV,listeDeltaW,repwcomplete,"W est de type "+W.cartan_type()[0]+str(W.cartan_type()[1])+"\n repw vaut "+str(repw)+u"\n v a pour représentant "+str(sousMotReduitMinimal(v,repw)),lettreW=lettrepourW,lettreV=lettrepourV)
	for i in range(1,len(mutationsAFaire)+1):#On affiche le résultat de chaque mutation
		listeDeltaV=mutaStratesDeltaVecteurs([Q,listeDeltaV],dDeltaV,mutationsAFaire[-i])[1]#On recalcule les delta_v vecteurs
		[Q,listeDeltaW]=mutaStratesDeltaVecteurs([Q,listeDeltaW],dDeltaW,mutationsAFaire[-i])#On recalcule le nouveau carquois et les delta_w vecteurs
		if i==1:
			texte=r'$\mu$'+str(mutationsAFaire[-i])+'(T)'
		else:
			texte=r'$\mu$'+str(mutationsAFaire[-i])+r'$\circ$'+texte
		[listeCarquois[2*i],listeCarquois[2*i+1]]=genCarquois(Q,listeDeltaV,listeDeltaW,repwcomplete,u"Mutation : "+texte,lettreW=lettrepourW,lettreV=lettrepourV)
	[listeCarquois[-2],listeCarquois[-1]]=genCarquois(Q,listeDeltaV,listeDeltaW,repwcomplete,u"Mutation : "+texte+u" correctement découpé\n"+u"On a retiré les sommets :"+str(listeSommetsAExclure),listeSommetsAExclure,lettreW=lettrepourW,lettreV=lettrepourV)
	k=len(listeCarquois)
	nbligne=ceil(k/4)
	G=graphics_array(listeCarquois,nbligne,4)
	for i in range(len(listeDeltaV)):#On regarde la liste des Delta_v vecteurs
		if not(i+1 in listeSommetsAExclure):#On regarde seulement les sommets
			#qui existent encore
			for j in range(v.length()):#On regarde coordonnée par coordonnée
				if listeDeltaV[i][j]!=0:
					print(i)
					print(listeDeltaV[i])
					print(j)
					print(u"ECHEC DE L'ALGORITHME")
					return([False,[Q,listeDeltaV,listeDeltaW],G,listeMouvements])
	if enregistrement:
		chemin=repertoireSauvegarde(repw,v)
		print("Les images ont été enregistrées dans "+os.path.expanduser(chemin))
		for i in range(len(listeCarquois)/2):
			save(G[2*i],os.path.expanduser(chemin+str(i)+'.png'))
			save(G[2*i+1],os.path.expanduser(chemin+str(i)+' coords.png'))
	return([True,[Q,listeDeltaV,listeDeltaW],G,listeMouvements])

def testAutomatique(W):
	"""
		Teste automatiquement des couples (repw,v) pour le type de W indiqué
		jusqu'à extinction par l'utilisateur. Sauvegarde dans un fichier
		horodaté les tests concluants et dans erreur.log les tests fautifs.
	"""
	[f,chemin]=creerFichierLog()
	f.write("\n On teste pour un groupe de Weyl de type "+str(W.cartan_type())+"\n ")
	f.close()
	import time
	cpt=0#Compte le nombre de tests
	while 1:
		t1=time.time()
		w=W.random_element()
		repw=motVersRep(w)
		v=choixSousMot(W,repw)
		print("repw est "+str(repw)+" v est "+str(v))
		[resultat,graineFinale,Graphe,listeMouvements]=testAlgo(repw,v)
		cpt=cpt+1
		if resultat:
			g=open(chemin,'a')
			t2=time.time()-t1
			g.write('Test n°'+str(cpt)+', il est '+recupererDate()+' Le test pour le représentant de w '+str(repw)+' et le mot v '+str(v)+' a été concluant. Il a pris '+str(t2)+' secondes.\n ')
			g.close()
		else:
			g=open(os.path.expanduser(cheminCourant+"log/"+"echec.log"),'a')
			t2=time.time()-t1
			import datetime
			now=datetime.datetime.now()
			res=now.strftime("%Y_%m_%d_%H_%M_%S")
			g.write('Il est '+res+'. Le test pour le groupe de Weyl de type '+str(W.cartan_type())+', de représentant de w '+str(repw)+' et le mot v '+str(v)+' a échoué. Il a pris '+str(t2)+' secondes.\n ')
			g.close()
	

def testAlgo2(repw,v,lettrepourW='e',lettrepourV='f',enregistrement=False,enregistrementSSVZ=False,couperADroite=False,couperAGauche=False,repv=[],listeMutations=[],mutationsCourtes=True):
	"""
		Prend en entrée le représentant repw d'un mot w d'un groupe de Weyl W
		un mot d'un groupe de Weyl v tel que v<w pour l'ordre de Bruhat dans W.
		Et retourne le carquois de la graine pour l'algèbre amassée de C_v,w
		calculée d'après l'algorithme de Bernard Leclerc où les sommets sont
		exprimés en fonction des Delta-vecteurs relatifs au complété de v.
		On pourra aussi spécifier une lettre pour indexer les delta_w vecteurs 
		et une pour indexer les delta_v vecteurs.
		L'option enregistrement prend en entrée un booléen et si celui ci
		vaut true, une copie de chacune des images sera enregistrée dans un 
		dossier indiqué.
		L'option enregistrementSSVZ prend en entrée un booléen et si celui ci
		vaut true, une copie de chacune des images du calcul des Delta-vecteurs
		sera enregistrée dans un dossier indiqué.
		L'option couperADroite permet, si elle vaut True, de n'afficher dans
		le module que les sommets n'étant pas encore dans la bonne catégorie.
		L'option couperAGauche permet, si elle vaut True, de supprimer au fur
		et à mesure les sommets d'indice maximaux. couperAGauche est incompatible
		avec mutationsCourtes. Si utilisés ensemble, mutationsCourtes est forcé à False.
	""" 
	W=v.parent()
	if not(v.bruhat_le(repVersMot(repw,W))):#protection
		print(u"v n'est pas plus petit que w pour l'ordre de Bruhat")
		return False
	if repv==[]:
		repvcomplete=completeMot(sousMotReduitMinimal(v,repw),W)
	else:
		repvcomplete=completeMot(repv,W)
	repwcomplete=completeMot(repw,W)
	tailleMaxStringInitiale=200
	tailleMaxString=tailleMaxStringInitiale#Savoir quand passer à la ligne dans texte
	#Mouvements SSVZ
	listeMouvements=calculMouvementsTresses(repvcomplete,repwcomplete,W)
	[repwinutile,graineIni,dDeltaV]=muteCarquoisSSVZ(repvcomplete,listeMouvements,W,affiche=False,enregistrement=enregistrementSSVZ)[:3]
	#Initialisation des données
	listeDeltaV=graineIni[1]
	listeDimDeltaW=calculDimMi2(W,repwcomplete)
	dDeltaW=vecteurdDelta(listeDimDeltaW,creerCarquois(W),repwcomplete)
	graineActuelle=deepcopy(graineIni)
	texte=''
	if listeMutations==[]:
		listeMutations=listeMuta2(repw,v)
	if not(couperAGauche):#On retire les suppressions de modules
		indicesSuppressionsARetirer=[]
		for i in range(len(listeMutations)):
			if listeMutations[i][1]==-1:
				indicesSuppressionsARetirer=indicesSuppressionsARetirer+[i]
		for i in range(len(indicesSuppressionsARetirer)-1,-1,-1):#On supprime à l'envers pour ne pas avoir de souci d'indice
			listeMutations.pop(indicesSuppressionsARetirer[i])#On retire cet élément
		mutationsCourtes=False
		print("Le booléen mutationsCourtes a été forcé à False")
	if mutationsCourtes :
		listeCarquois=[Graphics()]*(2*(v.length()+1))
		j=1#On définit une variable pour savoir où mettre les images
	else :
		listeCarquois=[Graphics()]*(2*(len(listeMutations)+2))#On rajoute le carquois
	#avant mutation et celui après, une fois qu'on a enlevé les sommets
	#et on aura un graphique pour les images, un pour les coordonnées
	listeExclusion=list(range(len(repw)+1,len(repwcomplete)+1))#On supprime
	#les modules supplémentaires du fait de la complétion de repw
	[Q,listeDeltaW]=graineInitiale(repwcomplete,W,1,False)
	[listeCarquois[0],listeCarquois[1]]=genCarquois2(Q,listeDeltaV,listeDeltaW,repwcomplete,listeDimDeltaW,"W est de type "+W.cartan_type()[0]+str(W.cartan_type()[1])+"\n repw vaut "+str(repw)+u"\n v a pour représentant "+str(sousMotReduitMinimal(v,repw)),exclusion=listeExclusion,lettreW=lettrepourW,lettreV=lettrepourV,couperADroite=couperADroite,v=v)
	if mutationsCourtes:
		for i in range(1,len(listeMutations)+1):#On affiche le résultat après chaque suppression de sommet
			if listeMutations[-i][1]==0:#On est sur une mutation
				listeDeltaV=mutaStratesDeltaVecteurs([Q,listeDeltaV],dDeltaV,listeMutations[-i][0])[1]#On recalcule les delta_v vecteurs
				[Q,listeDeltaW]=mutaStratesDeltaVecteurs([Q,listeDeltaW],dDeltaW,listeMutations[-i][0])#On recalcule le nouveau carquois et les delta_w vecteurs
				if i==1:
					if mutationsCourtes :
						texte=r'$\mu$'+str(listeMutations[-i][0])+'(T) court'#Générer des images légèrement différentes
					else :
						texte=r'$\mu$'+str(listeMutations[-i][0])+'(T)'
				else:
					texte=r'$\mu$'+str(listeMutations[-i][0])+r'$\circ$'+texte
					if len(texte)>tailleMaxString:#Texte est trop long, on passe à la ligne
						texte="\n"+texte
						tailleMaxString=tailleMaxString+tailleMaxStringInitiale
				#On n'ajoute pas les images à la liste
			elif listeMutations[-i][1]==-1:#On est sur une suppression de sommet
					texte='S('+str(listeMutations[-i][0])+r')$\circ$'+texte
					if len(texte)>tailleMaxString:#Texte est trop long, on passe à la ligne
						texte="\n"+texte
						tailleMaxString=tailleMaxString+tailleMaxStringInitiale
					listeExclusion=listeExclusion+[listeMutations[-i][0]] #On rajoute le sommet des sommets à supprimer
					[listeCarquois[2*j],listeCarquois[2*j+1]]=genCarquois2(Q,listeDeltaV,listeDeltaW,repwcomplete,listeDimDeltaW,r"Mutation : "+texte,exclusion=listeExclusion,lettreW=lettrepourW,lettreV=lettrepourV,v=v,couperADroite=couperADroite)
					j=j+1
	else :
		for i in range(1,len(listeMutations)+1):#On affiche le résultat de chaque mutation
			if listeMutations[-i][1]==0:#On est sur une mutation
				listeDeltaV=mutaStratesDeltaVecteurs([Q,listeDeltaV],dDeltaV,listeMutations[-i][0])[1]#On recalcule les delta_v vecteurs
				[Q,listeDeltaW]=mutaStratesDeltaVecteurs([Q,listeDeltaW],dDeltaW,listeMutations[-i][0])#On recalcule le nouveau carquois et les delta_w vecteurs
				if i==1:
					texte=r'$\mu$'+str(listeMutations[-i][0])+'(T)'
				else:
					texte=r'$\mu$'+str(listeMutations[-i][0])+r'$\circ$'+texte
					if len(texte)>tailleMaxString:#Texte est trop long, on passe à la ligne
						texte="\n"+texte
						tailleMaxString=tailleMaxString+tailleMaxStringInitiale
				[listeCarquois[2*i],listeCarquois[2*i+1]]=genCarquois2(Q,listeDeltaV,listeDeltaW,repwcomplete,listeDimDeltaW,r"Mutation : "+texte,exclusion=listeExclusion,lettreW=lettrepourW,lettreV=lettrepourV,v=v,couperADroite=couperADroite)
			elif listeMutations[-i][1]==-1:#On est sur une suppression de sommet
					texte='S('+str(listeMutations[-i][0])+r')$\circ$'+texte
					if len(texte)>tailleMaxString:#Texte est trop long, on passe à la ligne
						texte="\n"+texte
						tailleMaxString=tailleMaxString+tailleMaxStringInitiale
					listeExclusion=listeExclusion+[listeMutations[-i][0]] #On rajoute le sommet des sommets à supprimer
					[listeCarquois[2*i],listeCarquois[2*i+1]]=genCarquois2(Q,listeDeltaV,listeDeltaW,listeDimDeltaW,repwcomplete,r"Mutation : "+texte,exclusion=listeExclusion,lettreW=lettrepourW,lettreV=lettrepourV,v=v,couperADroite=couperADroite)
	[listeCarquois[-2],listeCarquois[-1]]=genCarquois2(Q,listeDeltaV,listeDeltaW,repwcomplete,listeDimDeltaW,r"Mutation : "+texte+" correctement decoupe",listeExclusion,lettreW=lettrepourW,lettreV=lettrepourV,couperADroite=couperADroite,v=v)
	k=len(listeCarquois)
	nbligne=ceil(k/4)
	G=graphics_array(listeCarquois,nbligne,4)
	#Définition des bons noms de fichiers
	if enregistrement:
		chemin=repertoireSauvegarde(repw,v)
		if mutationsCourtes :
			if couperADroite:
				if couperAGauche:
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeDroit coupeGauche "+str(i)+' court.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeDroit coupeGauche "+str(i)+' coords court.png'))
				else:
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeDroit "+str(i)+' court.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeDroit "+str(i)+' coords court.png'))
			else:
				if couperAGauche :
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeGauche"+str(i)+' court.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeGauche"+str(i)+' coords court.png'))
				else :
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+str(i)+' court.png'))
						save(G[2*i+1],os.path.expanduser(chemin+str(i)+' coords court.png'))
		else:
			if couperADroite:
				if couperAGauche :
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeDroit coupeGauche "+str(i)+'.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeDroit coupeGauche "+str(i)+'coords.png'))
				else :
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeDroit "+str(i)+'.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeDroit "+str(i)+'coords.png'))
			else:
				if couperAGauche:
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+"coupeGauche "+str(i)+'.png'))
						save(G[2*i+1],os.path.expanduser(chemin+"coupeGauche "+str(i)+' coords.png'))
				else :
					for i in range(len(listeCarquois)/2):
						save(G[2*i],os.path.expanduser(chemin+str(i)+'.png'))
						save(G[2*i+1],os.path.expanduser(chemin+str(i)+' coords.png'))
		print("Les images ont été enregistrées dans "+os.path.expanduser(chemin))
	if couperAGauche:#Si on ne coupe pas à gauche ça plante forcément
		for i in range(len(listeDeltaV)):#On regarde la liste des Delta_v vecteurs
			if not(i+1 in listeExclusion):#On regarde seulement les sommets
				#qui existent encore
				for j in range(v.length()):#On regarde coordonnée par coordonnée
					if listeDeltaV[i][j]!=0:
						print(i)
						print(listeDeltaV[i])
						print(j)
						print(r"ECHEC DE L'ALGORITHME")
						return([False,[Q,listeDeltaV,listeDeltaW],G,listeMouvements])
	
	return([True,[Q,listeDeltaV,listeDeltaW],G,listeMouvements])
