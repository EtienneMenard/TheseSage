# coding: utf8

def fonctionCoucoudeltaVecteurs():
	print("Fichier deltaVecteurs.sage chargé")
	

fonctionCoucoudeltaVecteurs()

def formeBilinNonSym(d1,d2,Q):
	"""
		Calcule la forme bilinéaire définie dans la section 2.1 de Kac-Moody
		groups and cluster Algebras pour deux N^r-vecteurs d1 et d2
		associée au carquois Q de sommets Q0 (liste d'entiers) et de flèches
		Q1 (liste de 2-uplets : (source,but) )
	"""
	[Q0,Q1]=Q
	resultat=0
	for i in Q0:
		resultat=resultat+d1[i-1]*d2[i-1]#-1 car la numérotation des listes commence à 0
		#et celle des sommets à 1
	for j in Q1:
		resultat=resultat-d1[j[0]-1]*d2[j[1]-1]
	return(resultat)
	
def formeBilinSym(d1,d2,Q):
	"""
		Calcule le symétrisé de formeBilinSym
	"""
	return(formeBilinNonSym(d1,d2,Q)+formeBilinNonSym(d2,d1,Q))
	#A optimiser éventuellement en évitant le double calcul des termes quadratiques
	#Mais ça risque de détruire la lisibilité

def calculDimHomVkMs(indexV,indexM,listeVecteursDimM,Q,w):
	"""
		Calcule Dim Hom_Lambda(V_k,M_s) d'après le lemme 9.8 de Kac-Moody
		groups and cluster Algebras en prenant en entrée l'index du module V_i,
		celui du module M_i (entiers), la liste des vecteurs dimensions de M_i,
		Q=[Q0,Q1] le carquois en question (vu comme un 2-uplet) et w le mot
		pour lequel on considère la delta-filtration
	"""
	resultat=0
	listekACalculer=[]
	if indexV<indexM:
		return(0)
	elif indexV==indexM:
		return(1)
	else:
		listekACalculer=listeTermeSommeCalculDimHom(indexV,indexM,w)
		if w[-indexV]==w[-indexM]:
			resultat=1
			for i in listekACalculer:
				resultat=resultat+formeBilinSym(listeVecteursDimM[i-1],listeVecteursDimM[indexM-1],Q)
		else:
			for i in listekACalculer:
				resultat=resultat+formeBilinSym(listeVecteursDimM[i-1],listeVecteursDimM[indexM-1],Q)
		return(resultat)
	
def listeTermeSommeCalculDimHom(indexV,indexM,repw):
	"""
		Calcule la liste indexée par m des indexV^(-m) tels que m>=0, et k^(-m)>indexM
	"""
	listePos=listePositions(repw,repw[-indexV])
	listePos2=copy(listePos)#Pour ne pas modifier la liste sur laquelle on indexe
	for k in listePos:
		if k<=indexM or k>indexV: 
			listePos2.remove(k)
	return listePos2
	
def calculDimDeltak(indexM,listeVecteursDimM,Q,repw):
	"""
		Calcule le vecteur dimension du module standard Delta_k
	"""
	vecteurDim=[]
	r=len(repw)
	coefficient=0
	for i in range(1,r+1):
		coefficient=calculDimHomVkMs(i,indexM,listeVecteursDimM,Q,repw)
		vecteurDim=vecteurDim+[coefficient]
	return vecteurDim
	
def vecteurdDelta(listeVecteursDimM,Q,repw):
	"""
		Calcule les vecteurs dimensions des Delta_k comme décrit à la section
		12.5 de 'Kac-Moody groups and cluster Algebras'
		Prend en entrée (listeVecteursDimM,Q,repw) où listeVecteursDimM est la
		liste des vecteurs dimensions des modules M_i (en tant que vecteurs 
		dimensions d'une représentation de carquois), Q=[Q0,Q1] est un carquois
		où Q0 est la liste des sommets, Q1 une liste de tuple (source,but)
		des flèches du carquois et repw le représentant du mot considéré.
	"""
	n=len(repw)
	resultat=[]
	for i in range(1,n+1):
		resultat=resultat+[calculDimDeltak(i,listeVecteursDimM,Q,repw)]
	return(resultat)

def calculDimMi(dimVi,repw):
	"""
		Calcule les vecteurs dimensions des M_i à partir des vecteurs
		dimension des V_i
	"""
	n=len(dimVi)
	res=[0]*n
	indiceprec=-1
	for i in range(1,n+1):
		indiceprec=kmoins(repw,i)
		if indiceprec==-1:
			res[i-1]=dimVi[i-1]
		else:
			res[i-1]=list(vector(dimVi[i-1])-vector(dimVi[indiceprec-1]))
	return(res)
	
