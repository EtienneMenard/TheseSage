# coding: utf8

def fonctionCoucouMutationCarquois():
	print ("Fichier mutationCarquois.sage chargé")
	
fonctionCoucouMutationCarquois()

def matriceAdjacence(Q):
	"""
		Prend en entrée un carquois Q=[Q0,Q1] où Q0 est une liste de sommets
		et Q1 une liste de tuples (source,destination) représentant les flèches
		et retourne la matrice d'adjacence du carquois
	"""
	n=len(Q[0]) #On récupère le nombre de sommets
	matAdj=zero_matrix(n)
	for i in Q[1]:
		#Comme d'habitude, on note les sommets à partir de 1 et la matrix est indexée
		#à partir de 0
		matAdj[i[0]-1,i[1]-1]=matAdj[i[0]-1,i[1]-1]+1
		matAdj[i[1]-1,i[0]-1]=-matAdj[i[0]-1,i[1]-1]#La matrice est antisymétrique
	return matAdj
	
def creationCarquois(M):#A corriger 
	"""
		Crée un carquois à partir d'une matrice d'adjacence (non nécessairement carrée)
	"""
	[m,n]=M.dimensions()
	Q0=range(1,m+1)
	Q1=[]
	for i in range(m):
		for j in range(n):
				if M[i,j]>0:
					for k in range(M[i,j]):#On rajoute autant d'arête que 
						#nécessaire
						Q1=Q1+[(i+1,j+1)]
				if M[i,j]<0 and i>n-1:
					for k in range(-M[i,j]):#Idem
						Q1=Q1+[(j+1,i+1)]
	return([Q0,Q1])

def mutationMatrice(M,k):
	"""
		Mute la matrice M dans la direction k
	"""
	[m,n]=M.dimensions()
	if k>n:
		print("On ne peut pas muter les projectifs")
		return M
	res=copy(M)
	for i in range(m):
		for j in range(n):
			if i==k-1 or j==k-1:#Ne pas oublier le décalage
				res[i,j]=-M[i,j]
			else:
				res[i,j]=M[i,j]+(abs(M[i,k-1])*M[k-1,j]+M[i,k-1]*abs(M[k-1,j]))/2
	return res
	
#Rajouter mutation directement à partir du carquois

def creationCarquois2(M):
	"""
		Crée un carquois arbitraire à partir de la matrice (carrée) d'adjacence de
		son diagramme de Dynkin avec toutes les flèches de la forme i->j
		avec i<j
	"""
	[m,n]=M.dimensions()
	Q0=range(1,m+1)
	Q1=[]
	for i in range(m):
		for j in range(i+1,n):
				if M[i,j]==1:
					Q1=Q1+[(i+1,j+1)]
	return([Q0,Q1])
	
def mutationCarquois(Q,k):
	"""
	Mute le carquois Q dans la direction k
	"""
	M=matriceAdjacence(Q)
	M2=mutationMatrice(M,k)
	return(creationCarquois(M2))
