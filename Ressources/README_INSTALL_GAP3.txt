0. Créer un dossier où l'on voudra installer le programme GAP3 (par exemple dans le dossier personnel ~, éviter de le faire dans /usr/bin ) en tapant "mkdir gap3". On appellera désormais ce dossier DOSSIERGAP
1. Déplacer le fichier gap3-jm30nov18.tar.gz dans le DOSSIERGAP précédemment créé (à récupérer ici : https://webusers.imj-prg.fr/~jean.michel/gap3/ le nom peut avoir changé)
2. Décompresser le fichier en tapant "tar xzf gap3-jm30nov18.tar.gz"
3. Aller dans le dossier DOSSIERGAP/gap3-jm/bin et ouvrir le fichier gap.sh
4. Une fois dans le fichier, retrouver la ligne 20 (normalement) où il est indiqué GAP_DIR=...
5. Remplacer ce qui suit GAP_DIR= par l'emplacement de DOSSIERGAP/gap3-jm
6. Se positionner dans ~/bin (s'il n'existe pas, le créer)
7. Créer un lien pour appeler GAP à partir de la console en tapant "ln -s DOSSIERGAP/gap3-jm/bin/gap.sh gap3"
8. Taper gap3 et vérifier que le programme se lance correctement.


Débogage : si le terminal dit que gap3 n'existe pas, taper "printenv" et vérifier que dans "PATH=" se trouve le dossier "/home/NOM_DUTILISATEUR/bin" si ce n'est pas le cas, se placer dans le dossier home ("cd ~") et ouvrir ".bashrc" (s'il n'existe pas, le créer). Ajouter la ligne "PATH=$PATH:~/bin"
