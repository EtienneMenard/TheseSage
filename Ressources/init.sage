#Fichier à placer dans le dossier ~/.sage
%colors Linux 
#Ne pas toucher au segment suivant
#----------------------------------------------------------------------------
def testChargement():
	try :
		load("~/Sage/TheseSage/run.sage")
		print("Les fichiers Sage personnalisés ont été chargés. Pour empêcher ceci, modifier le fichier ~/.sage/init.sage")
		%colors Linux 
		#Rendre sage plus lisible
	except:
		print("\033[31;1m************************\n\n Impossible de charger les fichiers personnalisés\n Vérifier l'adresse du fichier dans ~/.sage/init.sage\n\n************************")

#----------------------------------------------------------------------------
#
#Commenter la ligne suivante pour éviter de charger au démarrage les fichiers sage
testChargement()
