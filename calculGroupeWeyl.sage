# coding: utf8

def fonctionCoucouCalculGroupeWeyl():
	print ("Fichier calculGroupeWeyl.sage chargé")
	
fonctionCoucouCalculGroupeWeyl()

def calculDimMi2(W,w):
	"""
		Calcule les vecteurs dimensions des modules M_i en fonction de 
		W le groupe de Weyl considéré et w la décomposition choisie du
		mot w (liste d'entiers à lire de droite à gauche)
	"""
	r=W.cartan_type()[1]
	n=len(w)
	res=[[]]*n
	for i in range(1,n+1):
		res[i-1]=list(betaListeik(w,i,W))
	return(res)

def genAlpha(n):
	"""
		Génère les vecteurs dimensions des alpha_i dans leur base canonique
	"""
	tmp=vector([0]*n)
	res=[[]]*n
	for i in range(1,n+1):
		tmp[i-1]=1
		res[i-1]=tmp
		tmp=vector([0]*n)
	return(res)

def actionSi(vecteurAlpha,vecteurBeta,W):
	"""
		Applique l'action de la réflexion par rapport au vecteurBeta (un vecteur)
		sur le vecteurAlpha (un vecteur) dans le groupe de Weyl W
		Autrement dit s_beta(alpha)
	"""
	mat=CartanMatrix(W.cartan_type())
	return(vecteurAlpha-(vecteurBeta*mat*vecteurAlpha)*vecteurBeta)
	
def betaListeik(listei,indicek,W):
	"""
		Calcule le vecteur beta_i(k) du corollaire 9.2 de Kac-Moody...
	"""
	alpha=genAlpha((W.cartan_type())[1])
	if indicek==1:
		return(alpha[listei[-1]-1])
	tmp=alpha[listei[-indicek]-1]
	for i in range(indicek-1,0,-1):
		tmp=actionSi(tmp,alpha[listei[-i]-1],W)
	return(tmp)

def calculDimVi2(W,w):
	"""
		Donne la liste des vecteurs dimensions des modules Vi de la graine
		initiale de l'algèbre amassée associée au groupe de Weyl W et à la
		décomposition w (liste d'entiers)
		Nécessite d'avoir appelé le fichier mutationStrates.sage
	"""
	listevecteurs=[vector([])]*len(w)
	listeListesVecteurs=calculDimMi2(W,w)
	listeIndiceAAdditionner=[]
	listeResultat=[[]]*len(w)
	for i in range(len(w)):#On change le type des listes en vecteurs pour les additionner
		listevecteurs[i]=vector(listeListesVecteurs[i])
	listeVecteursVi=[[]]*len(w)
	for i in range(1,len(w)+1):#On va reconstruire les vecteurs dimensions des Vi en additionnant les bons Mi
		#Penser à avoir chargé mutationStrates.sage
		listeIndiceAAdditionner=listePositions(w[-i:],w[-i])#On récupère la liste des Mi à additionner
		vecteurTmp=vector([0]*(W.cartan_type()[1]))#On prépare le vecteur qu'on va former
		for j in listeIndiceAAdditionner:
			vecteurTmp=vecteurTmp+listevecteurs[j-1]
		listeResultat[i-1]=list(vecteurTmp)#On rechange le type pour avoir des listes
	return(listeResultat)
		
		
def motVersRep(w):
	"""
		Envoie un mot w d'un groupe de Weyl vers un représentant 
		(liste d'entiers) canonique (celui choisi par Sage pour le nommer)
	"""
	return(w.reduced_word())
	
def repVersMot(repw,W):
	"""
		Envoie un représentant repw (sous forme de liste d'entiers) d'un 
		mot d'un groupe de Weyl W  vers son mot dans le groupe de Weyl.
	"""
	sr=W.simple_reflections()
	res=W.one()#On initialise le produit à 1
	for i in range(len(repw)):
		res=res*sr[repw[i]]
	return(res)
	
def sousMotReduitMinimal(v,repw):
	"""
		Renvoie le représentant de v (liste d'entiers) le plus à droite
		parmi les sous-mots du représentant w (vu comme une liste d'entiers)
		d'un mot d'un groupe de Weyl.
	"""
	if not(v.bruhat_le(repVersMot(repw,v.parent()))):
		print(str(v)+u" n'est pas un sous-mot du représentant "+str(repw))
		return False
	res=[]
	W=v.parent()#On récupère le groupe de Weyl
	listeIndices=Jbarre(W,v,repw)#On récupère les indices des lettres à prendre
	for i in listeIndices:
		res=[repw[-i]]+res
	return(res)
	
def sousMotReduitMinimalAGauche(u,dotv):
	"""
		Renvoie le représentant de v (liste d'entiers) le plus à gauche
		parmi les sous-mots du représentant u (vu comme une liste d'entiers)
		d'un mot d'un groupe de Weyl.A
	"""
	if not(u.bruhat_le(repVersMot(dotv,u.parent()))):
		print(str(v)+u" n'est pas un sous-mot du représentant "+str(repw))
		return False
	res=[]
	W=u.parent()#On récupère le groupe de Weyl
	#En fait on va écrire les mots à l'envers et utiliser le travail déjà fait pour sousMotReduitMinimal
	repw=ALenvers(dotv)
	v=u^-1#Un des représentants de l'inverse d'un mot est ce mot écrit en miroir
	listeIndices=Jbarre(W,v,repw)#On récupère les indices des lettres à prendre
	for i in listeIndices:
		res=res+[repw[-i]]
	listeIndicesModifiee=[]
	n=len(dotv)
	for i in listeIndices:
		listeIndicesModifiee=[n-i+1]+listeIndicesModifiee
	return([res,listeIndicesModifiee])
	
def ALenvers(rep):
	"""
		Ecrit l'élément rep en miroir
	"""
	n=len(rep)
	res=[]
	for i in range(1,n+1):
		res=res+[rep[-i]]
	return res
	
def completeMot(repV,W):
	"""
		Etant donné un représentant repV (liste d'entiers) du mot v, 
		donne un représentant vComplet du mot w0 de longueur maxi de W 
		de telle sorte qu'un de ses sous-mots contigus les plus à droite 
		soit v. i.e. w0=av pour un certain a
	"""
	v=repVersMot(repV,W)
	w0=W.long_element()
	a=w0*v**-1
	return(motVersRep(a)+repV)
	

	
