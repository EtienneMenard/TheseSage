# coding: utf8

def fonctionCoucougvecteurs():
	print("Fichier gvecteurs.sage chargé")
	
fonctionCoucougvecteurs()

def gvecteursInitiaux(n):
	"""
	Renvoie la liste des g-vecteurs initiaux pour une graine à n éléments.
	Un g-vecteur est une liste contenant un vecteur à n coordonnées et un entier
	valant +/-1 selon la coloration du sommet associé
	"""
	res=[]
	for i in range(n):
		tmp=[vector([0]*n),+1]#+1 pour les sommets verts, -1 pour les rouges
		tmp[0][i]=1
		res=res+[tmp]
	return res

def colorationSommets(listevecteurs):
	"""
	Etant donné une liste de vecteurs, retourne la coloration de chacun 
	des sommets correspondants en le réécrivant comme un g-vecteur
	"""
	M=matrix(listevecteurs)
	n=len(listevecteurs)
	if M.det()==0:
		print ("Erreur : les vecteurs ne forment pas une famille libre")
		return False
	M=M.transpose()#On va chercher à résoudre le système MX=B pour un 
	#vecteur B arbitraire de N^n (ici (1,...,1) ) et X l'ensemble des coefficients
	#On pose donc X=M^-1 B
	B=matrix([1]*n).transpose() #Peut-être à remplacer par un vecteur aléatoire ?
	X=M^-1*B
	res=[]
	for i in range(n):
		if X[i][0]>0:
			res=res+[[listevecteurs[i],+1]]
		elif X[i][0]<0:
			res=res+[[listevecteurs[i],-1]]
		else:
			print("Impossible de déterminer la couleur")
			return False
	return(res)

def mutationGvecteurs(direction,Graine):
	"""
	Mute les g-vecteurs de listegvecteurs dans la direction indiquée selon
	le carquois Q indiqué. Retourne aussi le carquois muté
	"""
	listevecteursfinale=[]
	Q=Graine[0]
	listegvecteurs=Graine[1]
	for i in range(len(listegvecteurs)):
		listevecteursfinale=listevecteursfinale+[listegvecteurs[i][0]]
	Q1=Q[1]
	listeDeSommets=[]
	if listegvecteurs[direction-1][1]==1:
		for i in Q1:#Si on a une arête qui a pour but k, on ajoute sa source
			if i[1]==direction:
				listeDeSommets=listeDeSommets+[i[0]]
	elif listegvecteurs[direction-1][1]==-1:
		for i in Q1:
			if i[0]==direction:
				listeDeSommets=listeDeSommets+[i[1]]
	vecteurtmp=-listegvecteurs[direction-1][0]
	for i in listeDeSommets:
		vecteurtmp=vecteurtmp+listevecteursfinale[i-1]
	listevecteursfinale[direction-1]=vecteurtmp
	NouveauQ=mutationCarquois(Q,direction)
	return([NouveauQ,colorationSommets(listevecteursfinale)])

def mutaListeGVecteurs(listeMutations,Graine):
	"""
		Applique une suite de mutations à la graine de g-vecteurs Graine
	"""
	GraineActuelle=deepcopy(Graine)
	n=len(listeMutations)
	for i in range(1,n+1):
		GraineActuelle=mutationGvecteurs(listeMutations[-i],GraineActuelle)
	return(GraineActuelle)

def genCarquoisGvecteurs(Q,listeGvecteurs,repw,texte="",exclusion=[],affiche=False):
	"""
		Affiche le carquois Q=[Q0,Q1] avec les sommets Q0 aux coordonnées
		calculées à partir de repw et les étiquettes indiquées par listeGvecteurs.
		Sur le graphique est écrit la chaine de caractères texte passée en paramètre.
		La liste exclusion permet de choisir des sommets qu'on n'affichera pas.
		Le booléen affiche permet de choisir si l'on affiche ou pas le carquois.
	"""
	[Q0,Q1]=Q
	carquois=Graphics()
	coords=genCoordsCarquois(repw)
	[xmin,xmax,ymin,ymax,pas]=calculeDimensionsGraphiqueCarquois(coords)
	xmoy=(xmin+xmax)/2
	coords2=decalleCoord(coords,[0,5*pas])
	figcoords=Graphics()
	figcoords=figcoords+text("g-vecteurs",[xmoy,ymin+3*(len(Q0)+2)*pas],rgbcolor=(0,0,1),horizontal_alignment='center')
	for i in range(len(Q1)):#Création des flèches
		if not((Q1[i][0]in exclusion) or (Q1[i][1] in exclusion)):
			carquois=carquois+arrow2d(coords[Q1[i][0]-1],coords[Q1[i][1]-1],rgbcolor=(0,0.25,0.5),arrowsize=10)
	for i in range(len(Q0)):#Création des sommets (par dessus les flèches à cause du chevauchement)
		if i+1 in exclusion:#On représente différemment les points à exclure
			carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0,0,0),fontsize='x-large',horizontal_alignment='left')+point(coords[i],size=100,rgbcolor=(0,0,1))
			figcoords=figcoords+text('V'+str(i+1)+" : ",[xmoy,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,0,0),horizontal_alignment='center')#Report du nom des sommets sur la deuxième figure
		else :
			if listeGvecteurs[i][1]==1:
				carquois=carquois+point(coords[i],size=100,rgbcolor=(0,1,0))#On dessine le sommet
				carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(0,1,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				figcoords=figcoords+text('V'+str(i+1)+" : "+str(convertirCoordonnees(listeGvecteurs[i][0],'g')),[xmoy,ymin+3*(len(Q0)-i)*pas],rgbcolor=(0,1,0),horizontal_alignment='center')
			elif listeGvecteurs[i][1]==-1:
				carquois=carquois+point(coords[i],size=100,rgbcolor=(1,0,0))#On dessine le sommet
				carquois=carquois+text('V'+str(i+1),coords2[i],rgbcolor=(1,0,0),fontsize='x-large',horizontal_alignment='left')#On écrit le nom du sommet
				figcoords=figcoords+text('V'+str(i+1)+" : "+str(convertirCoordonnees(listeGvecteurs[i][0],'g')),[xmoy,ymin+3*(len(Q0)-i)*pas],rgbcolor=(1,0,0),horizontal_alignment='center')
				#Report du nom des sommets sur le graphique et écriture des g-vecteurs sur une autre figure
	listeAreteFormatee=''
	for i in range(len(Q1)):
		listeAreteFormatee=listeAreteFormatee+str(Q1[i])+' , '
		if i%10==9:#Ne pas avoir de liste trop longue
			listeAreteFormatee=listeAreteFormatee+"\n"
	figcoords=figcoords+text(u"liste des arêtes : \n"+listeAreteFormatee,[xmoy,ymin],rgbcolor=(0,0,1),horizontal_alignment='center',vertical_alignment='top')
	carquois=carquois+text(texte,[xmoy,ymin],rgbcolor=(1,0,0))
	carquois.axes(False)
	figcoords.axes(False)
	figcoords.axes_range(xmin,xmax,ymin,ymin+3*(len(Q0)+2)*pas)
	carquois.axes_range(xmin,xmax+3*pas,ymin,ymax)
	if affiche:
		G=graphics_array([carquois,figcoords],1,2)
		G.show()
	return([carquois,figcoords])
