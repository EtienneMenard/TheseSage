# coding: utf8

cheminCourant="~/Sage/TheseSage/" #Si nécessaire, remplacer par le chemin du dossier qui contient ce fichier run.sage
#En cas de doute, se positionner sur ce dossier dans le terminal linux et taper 'pwd'

load_attach_path(cheminCourant)

load("deltaVecteurs.sage")
load("mutationsStrates.sage")
load("mutationCarquois.sage")
load("visualisation.sage")
load("frontend.sage")
load("calculGroupeWeyl.sage")
load("coxeter.sage")
load("SSVZ.sage")
load("interfaceGap.sage")
load("IOFile.sage")
load("gvecteurs.sage")
load("mineursGeneralises.sage")
load("cvecteurs.sage")
#load("tests.sage")

print("\n Pour afficher de l'aide sur les fonctions principales, appeler aideMuta() (pas à jour)\n")
print("\n Pour avoir un exemple minimal fonctionnel, appeler initialisation()\n")


def aideMuta():
	print("Fonctions utiles : vecteur dDelta(dimM,Q,w) : construit les vecteurs dimensions des Delta_i\n")
	print("afficheCarquois(Q,coords,eti=[],dim=-1) : affiche  le carquois en question, si besoin avec les étiquettes de eti\n")
	print("genEtiquettesCarquois(w) : engendre la liste des Delta-vecteurs de la graine initiale associée au représentant w\n")
	print("listeMuta(W,v,w) : donne la liste des mutations conjecturées comme étant celles pour passer de C_w à C_v,w\n")
	print("graineInitiale(w,W,type=0) : génère la graine initiale associée au représentant w pour un groupe de Weyl W. Type 0 pour des étiquettes numériques, type 1 pour les Delta-vecteurs initiaux\n")
	print("mutaStratesDeltaVecteurs(Graine,dDelta,k) : mute la graine Graine=[Q,listeDeltaVecteurs] en ayant la liste dDelta des dim(Delta_i) dans la direction k\n")
	print("\n Pour afficher de l'aide sur les fonctions principales, appeler aideMuta()\n")
	print("\n Pour avoir un exemple minimal fonctionnel, appeler initialisation()\n")

#W=WeylGroup('A4',prefix='s')
#[s1,s2,s3,s4]=W.simple_reflections()
#v=s1*s2*s1
#w=[1,2,1,3,2,1,4,3,2,1]
#dimM=[[1,0,0,0],[1,1,0,0],[1,1,1,0],[1,1,1,1],[0,1,0,0],[0,1,1,0],[0,1,1,1],[0,0,1,0],[0,0,1,1],[0,0,0,1]]
#M=matrix([[0,1,0,0],[1,0,1,0],[0,1,0,1],[0,0,1,0]])
#Q0=range(1,5)
#Q1=[(1,2),(2,3),(3,4)]
#Q=[Q0,Q1]
#dDelta=vecteurdDelta(dimM,Q,w)
#QGraine=graineInitiale(w,W,1)
#listedi=genEtiquettesCarquois(w)

def pause():
	input("Presser entrée pour continuer\n\n")

def initialisation():
	print("Fonction de démonstration des capacités de l'algorithme. Les variables créées ne seront pas conservées à la fin de la fonction\n")
	pause()
	print("Le chemin courant est ",cheminCourant," si ce chemin est erroné, merci de modifier la ligne 3 du fichier run.sage")
	pause()
	print("On définit le groupe de Weyl sur lequel on va travailler")
	print("sage : [W,[s1,s2,s3,s4]]=creerGroupeWeyl('A4')")
	[W,[s1,s2,s3,s4]]=creerGroupeWeyl('A4')
	print("W est notre groupe de Weyl, ses générateurs sont les (si) 1<=i<=4")
	pause()
	print("On tire au sort un élément w de W")
	print("sage: w=W.random_element_of_length(8)")
	w=W.random_element_of_length(8)
	print("on l'affiche : \n sage: w\n")
	print(w)
	pause()
	print("Pour choisir un représentant particulier, on récupère son représentant canonique avec motVersRep(élément) et on le modifie avec modifAleatoireRep(représentant,NombreDeRéécrituresAléatoires,GroupeDeWeyl)")
	print("sage: repw=motVersRep(w)")
	repw=motVersRep(w)
	print("repw=",repw)
	print("sage: repw=modifAleatoireRep(repw,25,W)[0]")
	repw=modifAleatoireRep(repw,25,W)[0]
	print("repw=",repw)
	pause()
	print("On choisit un élément v plus petit que w pour l'ordre de Bruhat avec choixSousMot(W,repw). Attention : on peut avoir les cas triviaux v=w et v=e")
	print("sage: v=choixSousMot(W,repw)")
	v=choixSousMot(W,repw)
	v=choixSousMot(W,repw)
	print("v=",v)
	pause()
	print("Pour obtenir un exemple de notre algorithme pour les données v et repw choisies, on va utiliser la fonction testAlgo2")
	print("Par défaut testAlgo2 renvoie une liste de nombreuses données dont les graphiques et le carquois final")
	print("Par commodité, il est souvent plus simple de sauvegarder les images des carquois successifs")
	print("testAlgo2 prend en entrée deux arguments obligatoires : repw et v. repw est une liste d'entiers, v est un élément du groupe de Weyl")
	print("Ensuite il possède deux types d'arguments optionnels : les arguments permettant de modifier l'algorithme initial, et ceux configurant la sortie")
	print("Les arguments du premier type sont lettrePourW et lettrePourV qui donnent le nom des vecteurs de base des Delta_wpoint et Delta_vpoint vecteurs, repv qui permet d'indiquer un représentant non maximal à droite (au risque d'obtenir une sortie invalide")
	print("et listeMutations qui permet de définir soi même son propre algorithme de mutation")
	pause()
	print("Pour ce qui concerne la configuration de la sortie, le booléen enregistrement permet de sauvegarder les différents carquois de l'algorithme dans le dossier ",cheminCourant+"log")
	print("enregistrementSSVZ rajoute en plus les étapes de réécritures nécessaires à la détermination initiale des Delta_vpoint coordonnées de la graine initiaile (peut-être assez long et lourd en terme de place")
	print("couperADroite permet d'exclure des dessins de carquois les modules appartenant à Cv (qui apparaissent en points jaunes")
	print("couperAGauche permet de supprimer les modules d'indices maximums qui ne seront pas retenus dans la graine finale. Contrairement à la thèse, on les supprime au fur et à mesure des mutations")
	print("mutationsCourtes permet d'afficher uniquement les résultats des suites de mutations \hat{\mu_i} et de ne pas détailler toutes les mutations")
	pause()
	print("sage:testAlgo2(repw,v,mutationsCourtes=True,enregistrement=True)")
	testAlgo2(repw,v,mutationsCourtes=True,enregistrement=True)
	print("\n\n")
	print("On peut maintenant aller voir dans ",cheminCourant+"log/"+"A4/"+"repw="+str(repw)+"/v="+str(v)+"/", "la sortie de l'algorithme")
	pause()
	
	
	
